#ifndef POTENTIALFIELD_H
#define POTENTIALFIELD_H

#include "kfc.h"

using namespace std;

typedef struct JPOINT2
{
    double x;
    double y;
    JPOINT2() { }
    JPOINT2(double nX, double nY) : x(nX), y(nY){  }

    JPOINT2 operator^ (double ii)
    {
        if( ii == 0.5 ) // sqr root
        {
            double ret_x = 2, ret_y = 2;
            for(int i = 0 ; i < 10 ; i ++)
            {
                ret_x = (ret_x + (x / ret_x)) / 2;
                ret_y = (ret_y + (y / ret_y)) / 2;
            }
            return JPOINT2(ret_x, ret_y);
        }
        else if( ii > 1) // sqr
        {
            double ret_x = 1.0;
            double ret_y = 1.0;
            for(int i = 0 ; i < ii ; i++)
            {
                ret_x *= x;
                ret_y *= y;
            }
            return JPOINT2(ret_x, ret_y);
        }
    }

    JPOINT2 operator +(JPOINT2 p1)
    {
        JPOINT2 ret;
        ret.x = p1.x + this->x;
        ret.y = p1.y + this->y;
        return ret;
    }

    JPOINT2 operator -(JPOINT2 p1)
    {
        JPOINT2 ret;
        ret.x = this->x - p1.x;
        ret.y = this->y - p1.y;
        return ret;
    }

    JPOINT2 operator *(const int mul)
    {
        JPOINT2 ret;
        ret.x = mul * this->x;
        ret.y = mul * this->y;
        return ret;
    }
}jpoint2;


class PotentialField
{
public:
    void DrawObstacleMap(KArray<int>* kaSrc,KImageGray* igDst);
    bool GetPotentialField(KArray<int>* kaPotentialField, KArray<int>* kaGoalMap, KArray<int>* kaObsMap);
    // 힘만 따진거
    void GetPotentialField(KArray<double>* kaPotential_force,KArray<int>* kaPotential_direction,
                           KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[]);
    // 에너지로 따진거
    void GetPotentialField(KArray<double>* kaPotential_force,KArray<int>* kaPotential_direction,
                           KArray<double>* kaEnergyField, KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[]);
    void DisplayPotentialField(KImageColor* icMap, KArray<int>* kaPotentialField);
    JPOINT2 GetTranslationPoint(int i, int j, int direction, double length);

private:
    KArray<double> _kaRepulsiveField;
    KArray<double> _kaAttractiveField;

    KArray<double> _kaAttEnergyField;
    KArray<double> _kaRepEnergyField;
private: // function
    void MakeRepEnergy(KArray<int>* brushFire);
    void MakeAttEnergy(KArray<int>* brushFire);
    void MakePotentEngeryField(KArray<double>* energyField);

    int GetDirection_PotentialField(KArray<double>* kaPF_force, int i, int j);
    void MakePotentialDirection(KArray<double>* kaPF_force, KArray<int>* kaPF_direction);
    void MakePotentialForce(KArray<double>* kaPotential_force);
    void MakeRepulisveField(KArray<int>* kaObsMap);
    void MakeAttractiveField(KArray<int>* kaGoalMap);
    void BrushFire_ObstacleMap_repulsive(KArray<int>* kaObsMap);
    void BrushFire_Goal_attractive(KArray<int> *kaGoalMap, JPOINT2 goal[]);
    void MapIsObstacle(KArray<int>* kaObsMap);
    bool SetObstacle(KArray<int>* kaDst);
    //void DrawLine(int x1, int y1, int x2, int y2);
    //QColor()
};


class LikelihoodField
{
public:
    void KImageGray2KArray(KImageGray* igSrc, KArray<int>* kaDst);
    void BrushFire(KArray<int>* kaSrc); // brush fire대로 픽셀 업데이트
    void Execute(KArray<double>* kaLF, const KArray<int>* kaSrc, const double dGridSize, const double dSigmaHit,
                 const double dZ_hit, const double dZ_rand, const double dZ_max);
    void Display_LikelihoodField(KArray<double>* kaSrc, KImageGray* igDst);
private:
    double _dLfMax = -10000;
    double  _dLfMin = 10000;
protected:

};




#endif // POTENTIALFIELD_H
