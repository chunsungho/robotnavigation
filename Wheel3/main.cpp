
#include <iostream>

using namespace std;

#include <ode/ode.h>
#include <drawstuff/drawstuff.h>

#include "sensorBeam.h"


#ifdef _MSC_VER
#pragma warning(disable:4244 4305)  // for VC++, no precision loss complaints
#endif

// select correct drawing functions
#ifdef dDOUBLE
#define dsDrawBox      dsDrawBoxD
#define dsDrawSphere   dsDrawSphereD
#define dsDrawCapsule  dsDrawCapsuleD
#define dsDrawCylinder dsDrawCylinderD
#endif

#define NUM 4

//-------- my forward declaration----------//
JPOINT2 World2Pixelmap(JPOINT2 src);
double Direction2Angle(int direction);
void CalcRobotKinema(JPOINT2 robotPos_w, dReal dCurTheta, dReal& omega_r, dReal& omega_l);

//-----------------------------------------//

// ------- my global variation -----------//
const int nMapRow = 1000;
const int nMapCol = 1000;
KArray<double> g_kaPotential_force(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기
KArray<int> g_kaPotential_direction(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기
KArray<double> g_kaEnergyField(nMapRow,nMapCol);
//-----------------------------------------//

dWorldID world;
dSpaceID space;
dGeomID  ground;
dJointGroupID contactgroup;
dsFunctions fn;

KSensorLidar2D	_oLidar2D; //BeamSensor를 저장하는 Hash Map

typedef struct {
  dBodyID body;
  dGeomID geom;
  double  l,r,m;
} MyObject;

dReal   sim_step = 0.002;

MyObject wheel[NUM], base, neck, camera, ball;
dJointID joint[NUM], neck_joint, camera_joint;
static const dReal  BALL_R    = 0.15;
static const dReal  BALL_P[3] = {8.6, 7.0, 0.15};
dReal w_r = 0.0, w_l = 0.0 ;                   // 좌우 바퀴 속도
dReal BASE_M    = 9.4;                        // 베이스의 질량
dReal BASE_S[3] = {0.4,0.3,0.2};              // 베이스 크기

static const dReal  CAMERA_M  = 0.2;
static const dReal  CAMERA_S[3]  = {0.2,0.2,0.05};
static const dReal  NECK_M    = 0.5;
static const dReal  NECK_L    = 0.2;
static const dReal  NECK_R    = 0.025;

dReal WL;   //바퀴 축 길이
dReal WH_R0 = 0.05, WH_R1 = 0.10;            // 바퀴 반경
dReal WH_W  = 0.02, WH_M0 = 0.1, WH_M1 = 0.2; // 바퀴 폭, 질량
dReal START_X = 0, START_Y = 0, START_Z=0.20; // 초기위치

static dReal s_x = 0.0, s_y = 0.0, s_t = 0;     // deadreckoning



//-------------------my global variables
#define BOX_OBS_NUM     5
#define CYL_OBS_NUM     2
MyObject obstaBox[5];
dReal BOX_M = 100.0;
dReal BOX_S[5][3] = {{ 8, 2, 1},
                    {3,2,1},
                    {3,5,1},
                    {2,2,1},
                    {3,2,1}};

MyObject obstaCylin[2];
dReal cyl_W = 1, cyl_R = 1.5;
//-------------------

static void makeBall()
{
  dMass m1;

  dReal  ball_mass   = 0.0001;
  ball.body    = dBodyCreate(world);

  dMassSetZero(&m1);
  dMassSetSphereTotal(&m1,ball_mass,BALL_R);
  dMassAdjust(&m1,ball_mass);
  dBodySetMass(ball.body,&m1);

  ball.geom = dCreateSphere(space,BALL_R);
  dGeomSetBody(ball.geom,ball.body);
  dBodySetPosition(ball.body,BALL_P[0],BALL_P[1],BALL_P[2]);
}
// 다 같은데 lidar 부분만 추가됨
static void makeRobot()
{
  dMatrix3 R;
  dMass mass, mass2, mass3;

  //베이스
  base.body  = dBodyCreate(world);
  dMassSetZero(&mass);
  dMassSetBoxTotal(&mass,BASE_M,BASE_S[0],BASE_S[1],BASE_S[2]);
  dBodySetMass(base.body,&mass);

  base.geom = dCreateBox(space,BASE_S[0],BASE_S[1],BASE_S[2]);
  dGeomSetBody(base.geom,base.body);
  dBodySetPosition(base.body,START_X,START_Y,START_Z);

  //목
  neck.body  = dBodyCreate(world);
  dMassSetZero(&mass2);
  dMassSetCapsuleTotal(&mass2,NECK_M,3,NECK_R,NECK_L); //인수 3은 capsule 장축이 z축
  dMassAdjust(&mass2,NECK_M);
  dBodySetMass(neck.body,&mass2);

  neck.geom = dCreateCCylinder(space,NECK_R,NECK_L);
  dGeomSetBody(neck.geom,neck.body);
  dBodySetPosition(neck.body,START_X + 0.5*BASE_S[0] - NECK_R,
            START_Y, START_Z + 0.5*BASE_S[2] + 0.5*NECK_L);
  dGeomDisable(neck.geom);

  //카메라
  camera.body  = dBodyCreate(world);
  dMassSetZero(&mass3);
  dMassSetBoxTotal(&mass3,CAMERA_M,CAMERA_S[0],CAMERA_S[1],CAMERA_S[2]);
  dMassAdjust(&mass3,CAMERA_M);
  dBodySetMass(camera.body,&mass3);

  camera.geom = dCreateBox(space,CAMERA_S[0],CAMERA_S[1],CAMERA_S[2]);
  dGeomSetBody(camera.geom,camera.body);
  dBodySetPosition(camera.body,START_X + 0.5*BASE_S[0] - NECK_R, START_Y,
           START_Z + 0.5*BASE_S[2] + NECK_L);
  dGeomDisable(camera.geom);


  //바퀴
  dRFromAxisAndAngle(R,1,0,0, M_PI/2.0);
  for (int i = 0; i < NUM; i++) {
    wheel[i].body = dBodyCreate(world);

    dMassSetZero(&mass);
    if ((i % 2) == 0) {
      dMassSetCylinderTotal(&mass,WH_M0, 2, WH_R0, WH_W);
      dBodySetMass(wheel[i].body,&mass);
      wheel[i].geom = dCreateCylinder(space, WH_R0, WH_W);
    }
    else {
      dMassSetCylinderTotal(&mass,WH_M1, 2, WH_R1, WH_W);
      dBodySetMass(wheel[i].body,&mass);
      wheel[i].geom = dCreateCylinder(space, WH_R1, WH_W);
    }
    dGeomSetBody(wheel[i].geom, wheel[i].body);
    dBodySetRotation(wheel[i].body,R);
  }

  WL        = BASE_S[1]+WH_W;

  dReal w1y = 0.5 * (BASE_S[1] + WH_W);
  dReal w1z = START_Z - 0.5 * BASE_S[2];
  dReal w0x = 0.5 * BASE_S[0] - WH_R0;
  dReal w0z = START_Z - 0.5 * BASE_S[2] - WH_R0;

  dBodySetPosition(wheel[0].body,    w0x,  0, w0z);
  dBodySetPosition(wheel[1].body,    0,  w1y, w1z);
  dBodySetPosition(wheel[2].body,   -w0x,  0, w0z);
  dBodySetPosition(wheel[3].body,    0, -w1y, w1z);

  //힌지조인트
  for (int i = 0; i < NUM; i++) {
    joint[i] = dJointCreateHinge(world,0);
    dJointAttach(joint[i], base.body, wheel[i].body);
  }
  dJointSetHingeAxis(joint[0],0, -1, 0);
  dJointSetHingeAxis(joint[1],0, -1, 0);
  dJointSetHingeAxis(joint[2],0, -1, 0);
  dJointSetHingeAxis(joint[3],0, -1, 0);
  dJointSetHingeAnchor(joint[0],    w0x,  0, w0z);
  dJointSetHingeAnchor(joint[1],    0,  w1y, w1z);
  dJointSetHingeAnchor(joint[2],   -w0x,  0, w0z);
  dJointSetHingeAnchor(joint[3],    0, -w1y, w1z);

  //목 조인트
  neck_joint = dJointCreateHinge(world,0);
  dJointAttach(neck_joint,neck.body,base.body);
  dJointSetHingeAxis(neck_joint,0, 0, 1);
  dJointSetHingeAnchor(neck_joint, START_X + 0.5*BASE_S[0]-NECK_R,
             START_Y, START_Z + 0.5*BASE_S[0]);
  dJointSetHingeParam(neck_joint,dParamLoStop, 0);
  dJointSetHingeParam(neck_joint,dParamHiStop, 0);


  //카메라 조인트
  camera_joint = dJointCreateHinge(world,0);
  dJointAttach(camera_joint,neck.body,camera.body);
  dJointSetHingeAxis(camera_joint,1,0,0);
  dJointSetHingeAnchor(camera_joint,START_X + 0.5*BASE_S[0]-NECK_R,
              START_Y, START_Z + 0.5*BASE_S[0] + NECK_L);
  dJointSetHingeParam(camera_joint,dParamLoStop, 0.0);
  dJointSetHingeParam(camera_joint,dParamHiStop, 0.0);

  //Lidar 2D 센서
  KSENSORLIDAR2D_INFO	oInfo;
  OBJECT_ODE			oBase = { base.body, base.geom };	//Beam Sensor를 부착할 Object

  strcpy(oInfo.szID, "SensorLidar2D");
  oInfo.fR			= oInfo.fG = oInfo.fB = 1.0;
  oInfo.dX			= START_X + 0.5*BASE_S[0]; // 라이다를 쏘는 시작위치
  oInfo.dY          = START_Y;
  oInfo.dZ          = START_Z;
  oInfo.dMaxLength	= 3.0; // 라이다 max 거리는 3m로 초기화한다.
  oInfo.nLoRange	= -90;
  oInfo.nHiRange	= 90; // 180도 범위로 라이다를 쏜다
  oInfo.oBase       = oBase;

  _oLidar2D.Create(oInfo, world, space);
}

static void command(int cmd)
{
  switch (cmd) {
  case 'j': w_r += 0.1; break;
  case 'f': w_l += 0.1; break;
  case 'k': w_r -= 0.1; break;
  case 'd': w_l -= 0.1; break;
  case 's': w_r = w_l = 0.0;break;
  default: printf("\nInput j,f,k,d,l,s \n");break;
  }
}

static void control() {
  double fMax = 100;         // 최대 토크[Nm]

  dJointSetHingeParam(joint[1],  dParamVel, w_l );
  dJointSetHingeParam(joint[1], dParamFMax, fMax);
  dJointSetHingeParam(joint[3],  dParamVel, w_r );
  dJointSetHingeParam(joint[3], dParamFMax, fMax);
}

static void start()
{
  float xyz[3] = {  0.0f,   0.0f, 15.0f}; // viewpoint 위치 바꾸기
  float hpr[3] = { 0.0f, -90.0f, 0.0f}; // gaze 바꾸기

  dsSetViewpoint(xyz,hpr);
  dsSetSphereQuality(2);
}

static void nearCallback (void *data, dGeomID o1, dGeomID o2)
{ 

  dBodyID b1 = dGeomGetBody(o1);
  dBodyID b2 = dGeomGetBody(o2);

  if (b1 && b2 && dAreConnectedExcluding(b1, b2, dJointTypeContact)) return;

  static const int nMAX_CONTACT = 10;
  dContact contact[nMAX_CONTACT];

  //충돌점 계산
  int nNumContact = dCollide(o1, o2, nMAX_CONTACT, &contact[0].geom, sizeof(dContact));

  //센싱
  //이 조건문에 안걸리면 라이다에 뭔가가 충돌한거야.
  if(nNumContact == 0 || _oLidar2D.Measure(contact[0].geom) == true) // 충돌점이 0 : 충돌 안하고 있음.
      return;


  //총돌 설정
  for(int i=0; i<nNumContact; i++)
  {
      contact[i].surface.mode = dContactSlip1 | dContactSlip2 | dContactSoftERP | dContactSoftCFM | dContactApprox1;
      contact[i].surface.mu = dInfinity;
      contact[i].surface.slip1 = 0.1;
      contact[i].surface.slip2 = 0.1;
      contact[i].surface.soft_erp = 0.8;
      contact[i].surface.soft_cfm = 1e-5;

      dJointAttach(dJointCreateContact(world, contactgroup, &contact[i]), b1, b2);
  }
}

static void drawBall()
{
  dsSetColor(0.0,1.0,1.0);
  dsDrawSphere(dGeomGetPosition(ball.geom),
           dGeomGetRotation(ball.geom),BALL_R);
}

void makeObsta()
{
    dReal x_box[5] = {1,5.5,5.5,1,-1.5}, y_box[5] = {2,6,-4.5,-5,-6}, z_box[5] = {0.5,0.5,0.5,0.5,0.5};
    //dMatrix3 R;
    dMass mass;

    for(int i = 0 ; i < BOX_OBS_NUM ; i ++)
    {
      obstaBox[i].body = dBodyCreate(world); // body 생성
      dMassSetZero(&mass); // 질량 설정
      dMassSetBoxTotal(&mass,BOX_M, BOX_S[i][0], BOX_S[i][1], BOX_S[i][2]);
      dBodySetMass(obstaBox[i].body,&mass);

      obstaBox[i].geom = dCreateBox(space, BOX_S[i][0], BOX_S[i][1], BOX_S[i][2]); // 충돌을 위한 바디설정
      dGeomSetBody(obstaBox[i].geom, obstaBox[i].body); // 충돌을 위한 바디 설정
      dBodySetPosition(obstaBox[i].body,x_box[i],y_box[i],z_box[i]); // 장애물 위치 설정
    }

    dReal x_cyl[2] = {-4.5,-5.5}, y_cyl[2] = {5.5,-3.5}, z_cyl[2] = {0.5,0.5};
    for(int i = 0 ; i < CYL_OBS_NUM; i++)
    {
        obstaCylin[i].body = dBodyCreate(world);
        dMassSetZero(&mass);
        dMassSetCylinderTotal(&mass,BOX_M,1,cyl_R, cyl_W);
        dBodySetMass(obstaCylin[i].body,&mass);

        obstaCylin[i].geom = dCreateCylinder(space,cyl_R,cyl_W);
        dGeomSetBody(obstaCylin[i].geom, obstaCylin[i].body); // 충돌을 위한 바디 설정
        dBodySetPosition(obstaCylin[i].body,x_cyl[i],y_cyl[i],z_cyl[i]); // 장애물 위치 설정
    }
}

void drawObsta()
{
    dsSetColor(1.0,0.0,0.0);
    for(int i = 0 ; i < BOX_OBS_NUM ; i++)
    {
        dsDrawBox(dGeomGetPosition(obstaBox[i].geom),
              dGeomGetRotation(obstaBox[i].geom),BOX_S[i]);
    }

    for(int i = 0 ; i < CYL_OBS_NUM; i++)
    {
        dsDrawCylinder(dBodyGetPosition(obstaCylin[i].body),
                       dBodyGetRotation(obstaCylin[i].body),cyl_W,cyl_R);
    }

}

static void drawRobot()
{
  //베이스
  dsSetColor(1.3,1.3,0.0);
  dsDrawBox(dGeomGetPosition(base.geom),
        dGeomGetRotation(base.geom),BASE_S);

  //목
  dsSetColor(1.3,0.0,1.3);
  dsDrawCylinder(dBodyGetPosition(neck.body),
             dBodyGetRotation(neck.body),NECK_L,NECK_R);

  //카메라
  dsSetColor(0,1.0,0);
  dsDrawBox(dGeomGetPosition(camera.geom),
        dGeomGetRotation(camera.geom),CAMERA_S);

  //바퀴
  dsSetColor(1.1,1.1,1.1);
  for (int i=0; i< NUM; i++) {
        if ((i % 2) == 0) {
        dsDrawCylinder(dBodyGetPosition(wheel[i].body),
             dBodyGetRotation(wheel[i].body),WH_W,WH_R0);
    }
    else {
        dsDrawCylinder(dBodyGetPosition(wheel[i].body),
             dBodyGetRotation(wheel[i].body),WH_W,WH_R1);
        }
  }

  // 거리센서 
//  for(auto& beam : _oLidar2D)
//      beam.second->Draw();
}

//월드좌표계를 로봇좌표계로 변환
static void worldToRobot(const dReal p[2], const dReal c[2], dReal theta,
  dReal pr[2], dReal *r, dReal *phi)
{
  pr[0] =   (p[0]-c[0]) * cos(theta) + (p[1]-c[1]) * sin(theta);
  pr[1] = - (p[0]-c[0]) * sin(theta) + (p[1]-c[1]) * cos(theta);

  *r   = sqrt(pr[0] * pr[0] + pr[1] * pr[1]);  //물체까지의 거리
  *phi = atan2(pr[1], pr[0]);                  //물체의 각도
}

//로봇방향
dReal heading()
{
  const dReal *c,*cam;

  c   = dBodyGetPosition(base.body);     //베이스 월드좌표
  cam = dBodyGetPosition(camera.body);   //카메라 월드좌표

  return atan2(cam[1]-c[1], cam[0]-c[0]); // 로봇방향
}

//로봇 비전 함수
static int vision(dReal *r, dReal *phi, dBodyID obj_body)
{
  const dReal *p,*c,*cam;
  dReal pr[2], view_angle = M_PI/4.0;  //로봇좌표계에서의 물체위치,시야갹

  p   = dBodyGetPosition(obj_body);    //물체의 월드좌표
  c   = dBodyGetPosition(base.body);   //베이스의 월드좌표
  cam = dBodyGetPosition(camera.body); //카메라의 월드좌표

  dReal theta  = atan2(cam[1] - c[1], cam[0] - c[0]); //로봇 방향

  worldToRobot(p, c, theta, pr, r, phi);

  if ((-view_angle <= *phi) && (*phi <= view_angle)) return 1;
  else                                               return 0;
}

//바퀴 제어
static void controlWheel(dReal left, dReal right)
{
  double fMax = 100;

  dJointSetHingeParam(joint[1],  dParamVel, left);
  dJointSetHingeParam(joint[1], dParamFMax, fMax);
  dJointSetHingeParam(joint[3],  dParamVel, right);
  dJointSetHingeParam(joint[3], dParamFMax, fMax);
}

//회전
static void turn(dReal speed)
{
  controlWheel(speed, -speed);
}

//추적
static void track(dReal phi, dReal speed)
{
  dReal k = 0.5;
  if (phi > 0) controlWheel(k * speed,      speed);
  else         controlWheel(    speed,  k * speed);
}

#define JDEBUG
static void deadreckoning(dReal& s_x, dReal& s_y, dReal& s_t)
{
    const dReal  *c;
    dReal        theta;  // 로봇 방향
    dReal        omega_r, omega_l;
    dReal        fx, fy, ft;
    static dReal omega_l_old = 0, omega_r_old = 0, theta_old = 0;

    //로봇 월드좌표와 방향
    c     = dBodyGetPosition(base.body); // 실제 로봇의 위치
    theta = heading(); // radian

    //각속도
    omega_r = dJointGetHingeAngleRate(joint[3]);
    omega_l = dJointGetHingeAngleRate(joint[1]);

    //---- s_x, s_y, s_t 가 deadReckoning으로 추정한 로봇의 위치.
    fx      = 0.5 * WH_R1 * ((omega_r + omega_l) * cos(theta) + (omega_r_old + omega_l_old) * cos(theta_old) );
    fy      = 0.5 * WH_R1 * ((omega_r + omega_l) * sin(theta) + (omega_r_old + omega_l_old) * sin(theta_old) );
    ft      = 0.5 * WH_R1/WL* ( (omega_r - omega_l) + (omega_r_old - omega_l_old) );

    s_x    += fx * sim_step * 0.5;
    s_y    += fy * sim_step * 0.5;
    s_t    += ft * sim_step * 0.5;

    omega_r_old = omega_r;
    omega_l_old = omega_l;
    theta_old   = theta;

//    printf("Real x = %f  Dead Reckoning s_x = %f Error = %f\n", c[0], s_x, c[0] -s_x);
//    printf("Real y = %f  Dead Reckoning s_y = %f Error = %f\n", c[1], s_y, c[1] -s_y);
//    printf("Real t = %f  Dead Reckoning s_t = %f Error = %f\n", theta,s_t, theta-s_t);

}

static void simLoop(int pause)
{
  dReal        r, phi; // r:물체까지의 거리, phi: 물체의 방위각
  const dReal  *c;
  dReal        theta;  // 로봇 방향
  dReal        omega_r, omega_l;

    static int nLoopCnt = 0;

  if(!pause)
  {

//    for (auto& beam : _oLidar2D)
//        beam.second->_dRange = 3;         // 빔이 뻗을 수 있는 거리 초기화

     dSpaceCollide (space,0,&nearCallback); //

//     if (vision(&r, &phi, ball.body))
//     {
//          printf("tracking phi=%.2f \n",phi * 180/M_PI);
//          track(phi,4.0);
//     }
//     else
//     {
//          printf("lost the ball phi=%.2f \n",phi * 180/M_PI);
//          turn(4.0);
//     }

     dWorldStep(world, sim_step);
     dJointGroupEmpty (contactgroup);

     ////// my code start
     if((nLoopCnt % 10) == 0)
     {
         c     = dBodyGetPosition(base.body); // 실제 로봇의 위치
         theta = heading(); // radian
         CalcRobotKinema(JPOINT2(c[0],c[1]),theta, omega_r, omega_l);
         controlWheel(omega_l, omega_r);
     }
     nLoopCnt++;

     ////// my code end
     deadreckoning(s_x, s_y, s_t);
  }

  drawBall();
  drawRobot();

  drawObsta();
}

static void setDrawStuff() {
  fn.version = DS_VERSION;
  fn.start   = &start;
  fn.step    = &simLoop;
  fn.command = &command;
  fn.path_to_textures = "c:/ode-0.13/drawstuff/textures";
}

// world -> potential Field 좌표계로 변환
JPOINT2 World2Pixelmap(JPOINT2 src)
{
    // KMatrix 활용
    KMatrix T_P_W(4,4);
    T_P_W._ppA[0][0] = 0;       T_P_W._ppA[0][1] = -1;        T_P_W._ppA[0][2] = 0;       T_P_W._ppA[0][3] = 10;
    T_P_W._ppA[1][0] = -1;       T_P_W._ppA[1][1] = 0;       T_P_W._ppA[1][2] = 0;       T_P_W._ppA[1][3] = 10;
    T_P_W._ppA[2][0] = 0;       T_P_W._ppA[2][1] = 0;       T_P_W._ppA[2][2] = -1;       T_P_W._ppA[2][3] = 0;
    T_P_W._ppA[3][0] = 0;       T_P_W._ppA[3][1] = 0;       T_P_W._ppA[3][2] = 0;       T_P_W._ppA[3][3] = 1;

    KVector vODE(4); // 매트릭스 연산을 위해 4x1 행렬에 할당
    vODE[0] = src.x;
    vODE[1] = src.y;
    vODE[2] = 0;
    vODE[3] = 1;

    KVector vPX = T_P_W * vODE; // 결과 : 4x1 행렬

    return JPOINT2(vPX[0], vPX[1]); // return (x,y)
}

// 해당 좌표를 받고서 potential field를 고려하여 robot v,w를 계산하고
// 그것에 따른 바퀴 각속도를 계산해서 참조자로 내보낸다.
///////////
/// \brief CalcRobotKinema : 로봇의 좌표에 따른 v,w omega_r, omega_l 계산
///
void CalcRobotKinema(JPOINT2 robotPos_w, dReal dCurTheta, dReal& omega_r, dReal& omega_l)
{
#ifdef JDEBUG
//    static dReal dV_old = 0;
//    static double dForce_old = 0;
//    static int nDirection_old = 0;
#endif

    JPOINT2 robotPos_px = World2Pixelmap(robotPos_w) * 50;
    int i = (int)(robotPos_px.y + 0.5);  //0.5는 반올림
    int j = (int)(robotPos_px.x + 0.5);

    dReal dDesiredTheta = Direction2Angle(g_kaPotential_direction._ppA[i][j]);// direction 을 각도로 나타내고 싶다.

    assert(i+1 < g_kaEnergyField.Row() && i-1 >= 0 && j+1 < g_kaEnergyField.Col() && j-1 >= 0); // 맵 안에 있어야.

    //----- v, w 구하기
    const double dGridSize = 0.01;
    dReal dU_dqx = (g_kaEnergyField[i][j+1] - g_kaEnergyField[i][j-1])/(2.*dGridSize); // x축방향
    dReal dU_dqy = (g_kaEnergyField[i+1][j] - g_kaEnergyField[i-1][j])/(2.*dGridSize);
//    dReal Fx = g_kaPotential_force[i][j+1] - g_kaPotential_force[i][j-1]; // x축방향
//    dReal Fy = g_kaPotential_force[i+1][j] - g_kaPotential_force[i-1][j]; // 이게 맞는건지 아직 잘 모르겠다.

    const dReal Kp_w = 0.72; // 로봇이 z축 방향으로 너무 확확 돌면 줄인다.
    const dReal Kp_v = 0.031; // 로봇이 너무 급발진하면 줄인다.
    const dReal dW = Kp_w*(dDesiredTheta - dCurTheta); // 로봇의 w
    const dReal dV = Kp_v*(dU_dqx*sin(dCurTheta) + dU_dqy*cos(dCurTheta)); // theta가 y축방향 시작이라서.

    //------- omega_r, omega_l 구하기
    dReal dDistbetweenWheels = BASE_S[1] + WH_W;
    KMatrix mRadius(2,2);
    mRadius._ppA[0][0] = WH_R1/2.0;                          mRadius._ppA[0][1] = WH_R1/2.0;
    mRadius._ppA[1][0] = WH_R1/dDistbetweenWheels;   mRadius._ppA[1][1] = -WH_R1/dDistbetweenWheels;

    KVector vVW(2);
    vVW[0] = dV;
    vVW[1] = dW;

    KVector vOmega = mRadius.Iv() * vVW; // omega_l, omega_r 구한다.
    omega_r = vOmega[0];
    omega_l = vOmega[1];

#ifdef JDEBUG

    //printf("robot position in PX map(i,j) :(%d, %d)\n", i,j);
//    printf("desired theta : %lf, current theta : %lf", dDesiredTheta, dCurTheta);// 현재위치에서 desired theta, theta
//    printf("(V,W) : %lf, %lf\n",dV, dW);
//    printf("dU_dq(x,y) : %lf, %lf\n", dU_dqx, dU_dqy);
//    printf("omega(r,l) : %lf, %lf\n", omega_r, omega_l);
#endif

}
double Direction2Angle(int direction)
{
    double dAngle = 0.0;
    assert(direction != 0);


    switch (direction) {
    case 1:
        dAngle = 0.;
        break;

    case 2:
        dAngle = -1/4.*_PI;
        break;

    case 3:
        dAngle = -1/2.*_PI;
        break;

    case 4:
        dAngle = -3/4.*_PI;
        break;

    case 5:
        dAngle = _PI;
        break;

    case 6:
        dAngle = 3/4.*_PI;
        break;

    case 7:
        dAngle = 1/2.*_PI;
        break;

    case 8:
        dAngle = 1/4.*_PI;
        break;

    default:
        break;
    } // switch

    return dAngle;
}

int main(int argc, char *argv[])
{

    PotentialField pf;
    KArray<int> kaObsMap(nMapRow,nMapCol);      // 척력맵 그릴때
    KArray<int> kaGoalMap(nMapRow,nMapCol);     // 인력맵 그릴때

//    KArray<double> kaPotential_force(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기
//    KArray<int> kaPotential_direction(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기

    JPOINT2 goal[2];
    goal[0].x = 150;    goal[0].y = 70;
    goal[1].x = 950;    goal[1].y = 950;

    pf.GetPotentialField(&g_kaPotential_force,&g_kaPotential_direction,&g_kaEnergyField,
                         &kaObsMap,&kaGoalMap,goal); // potential Field 제작
    //pf.GetPotentialField(&g_kaPotential_force,&g_kaPotential_direction,&kaObsMap,&kaGoalMap,goal); // potential Field 제작


  dInitODE();
  setDrawStuff();

  world        = dWorldCreate();
  space        = dHashSpaceCreate(0);
  contactgroup = dJointGroupCreate(0);
  ground       = dCreatePlane(space,0,0,1,0);

  dWorldSetGravity(world, 0, 0, -9.8);

  makeRobot();
  makeBall();
  makeObsta();
  dsSimulationLoop(argc,argv,640,480,&fn);

  dSpaceDestroy(space);
  dWorldDestroy(world);
  dCloseODE();
  return 0;


}
