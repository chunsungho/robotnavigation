#include "potentialfield.h"


//----------------------------------------- potential field


// 새로
void PotentialField::GetPotentialField(KArray<double>* kaPotential_force, KArray<int>* kaPotential_direction, KArray<double> *kaEnergyField,
                       KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[])
{
    BrushFire_ObstacleMap_repulsive(kaObsMap); // 장애물맵 brush fire
    BrushFire_Goal_attractive(kaGoalMap,goal); // goal맵 brush fire

    MakeRepulisveField(kaObsMap); // brush fire 한 map에서 장애물과의 거리를 참고해서 척력맵을 그린다.
    MakeAttractiveField(kaGoalMap); // brush fire한 map에서 goal과의 거리를 참고해서 인력맵을 그린다.

    MakeRepEnergy(kaObsMap);
    MakeAttEnergy(kaGoalMap);
    MakePotentEngeryField(kaEnergyField);

    MakePotentialForce(kaPotential_force); // 여기까지는 값이 제대로 들어간다.
    MakePotentialDirection(kaPotential_force,kaPotential_direction);
}
#define JDEBUG
//새로
void PotentialField::MakeRepEnergy(KArray<int>* brushFire)
{
    const double gridSize = 0.01;
    const double dThresDist = 70 * gridSize;
    const double K_rep = 20.0;

    _kaRepEnergyField.Create(brushFire->Row(), brushFire->Col());

    assert(_kaRepEnergyField.Row() == brushFire->Row() && _kaRepEnergyField.Col() == brushFire->Col()); // map 크기 같아야함

    // 모든 grid에 척력 저장한다.
    for(int i = 0 , ii = _kaRepEnergyField.Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = _kaRepEnergyField.Col() ; j < jj ; j++)
        {
            double dObsDist = 1. * gridSize; // kaObsMap->_ppA[i][j] == 0 일때를 대비한 초기화
            if(brushFire->_ppA[i][j] != 0)
            {
                dObsDist = brushFire->_ppA[i][j] * gridSize; // 현재 픽셀에서 장애물까지의 거리
            }


            if(dObsDist <= dThresDist) // 장애물과 거리가 일정거리 이하일때만 척력이 작용한다.
            {
                _kaRepEnergyField._ppA[i][j] = 0.5 * K_rep * _SQR(1./dObsDist - 1./dThresDist);
            }
            else // 장애물과 거리가 멀면 척력 작용 x
            {
                _kaRepEnergyField._ppA[i][j] = 0;
            }
        }
    }

#ifdef JDEBUG
//    for(int i =500; i < 505 ; i ++)
//    {
//        for(int j = 463 ; j < 468 ; j++)
//        {
//            qDebug() << "척력 brushfire i = " << i << "j = "<< j<< " " << brushFire->_ppA[i][j];
//        }
//    }
#endif
}

// 새로
void PotentialField::MakeAttEnergy(KArray<int>* brushFire)
{
    const double K_att = 10.0;
    const double dGridSize = 0.01;

    _kaAttEnergyField.Create(brushFire->Row(), brushFire->Col());

    assert(_kaAttEnergyField.Row() == brushFire->Row() && _kaAttEnergyField.Col() == brushFire->Col()); // map 크기 같아야함

    // 모든 grid에 인력 저장한다.
    for(int i = 0 , ii = _kaAttEnergyField.Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = _kaAttEnergyField.Col() ; j < jj ; j++)
        {
            double dGoalDist = brushFire->_ppA[i][j] * dGridSize; // goal 까지의 거리
            _kaAttEnergyField._ppA[i][j] = 0.5 * K_att * _SQR(dGoalDist);
        }
    }

#ifdef JDEBUG
//    for(int i =500; i < 505 ; i ++)
//    {
//        for(int j = 463 ; j < 468 ; j++)
//        {
//            qDebug() << "인력 brushfire i = " << i << "j = "<< j<< " " << brushFire->_ppA[i][j];
//        }
//    }
#endif

}

//새로
void PotentialField::MakePotentEngeryField(KArray<double>* energyField)
{
    assert(energyField->Row() > 0 && energyField->Col() > 0);

    for(int i = 0 , ii = energyField->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = energyField->Col() ; j < jj ; j++)
        {
            energyField->_ppA[i][j] = _kaAttEnergyField._ppA[i][j] + _kaRepEnergyField._ppA[i][j];
        }
    }

#ifdef JDEBUG
//    qDebug() << "장애물 주변에서의 에너지";
//    for(int i =500; i < 505 ; i ++)
//    {
//        for(int j = 463 ; j < 468 ; j++)
//        {
//            qDebug() << "(i,j)" << i << j << energyField->_ppA[i][j];
//        }
//    }

//    qDebug() << "장애물 없는곳에서의 에너지";
//    for(int i =500; i < 505 ; i ++)
//    {
//        for(int j = 500 ; j < 505 ; j++)
//        {
//            qDebug() << "(i,j)" << i << j << energyField->_ppA[i][j];
//        }
//    }

#endif

}

/////////////
/// \brief PotentialField::BrushFire_ObstacleMap_repulsive : 장애물의 위치를 받아와서 장애물 brushfire그린다.
///                                                             장애물에서 멀어질수록 값은 5씩 감소
///                                                             로봇은 에너지가 큰쪽에서 멀어지고 작은쪽으로 가려는 성질을 이용한다.
/// \param kaObsMap
///
void PotentialField::BrushFire_ObstacleMap_repulsive(KArray<int>* kaObsMap)
{
    SetObstacle(kaObsMap); // kaObs로 장애물맵을 받아온다. brushfire 하기 전 밑작업

    const int nFreeSpace_ppA = 10000;

    for(int i = 0 , ii = kaObsMap->Row(); i < ii ; i++)
        for(int j = 0 , jj = kaObsMap->Col(); j < jj ; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if(kaObsMap->_ppA[i][j] < nFreeSpace_ppA)
            {
               //위
                if( i-1 >= 0 && kaObsMap->_ppA[i-1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i-1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaObsMap->_ppA[i+1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i + 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaObsMap->_ppA[i][j-1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j-1] = kaObsMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaObsMap->_ppA[i][j+1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j+1] = kaObsMap->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    for(int i = kaObsMap->Row() - 1, ii = kaObsMap->Row(); i >= 0 ; i--)
        for(int j = kaObsMap->Col()-1, jj = kaObsMap->Col() ; j >= 0 ; j--)
        {
            if(kaObsMap->_ppA[i][j] < nFreeSpace_ppA)
            {
               //위
                if( i-1 >= 0 && kaObsMap->_ppA[i-1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i-1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaObsMap->_ppA[i+1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i + 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaObsMap->_ppA[i][j-1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j-1] = kaObsMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaObsMap->_ppA[i][j+1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j+1] = kaObsMap->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

///////////
/// \brief PotentialField::BrushFire_Goal_attractive : target 정해주면 거기로부터 멀어지면서 커져야한다.
///                                                     가장 에너지가 작은곳이 인력의 근원이 된다. 멀어질수록 kaGoalMap에 적히는 숫자는 커진다.
/// \param kaGoalMap : 이 배열에 값이 업데이트된다.
///
void PotentialField::BrushFire_Goal_attractive(KArray<int> *kaGoalMap, JPOINT2 goal[])
{
    const int init_ppA = 10000;

    // 맵 전체를 우선 init_ppA로 초기화
    for(int i = 0 , ii = kaGoalMap->Row(); i < ii ; i++)
        for(int j = 0 , jj = kaGoalMap->Col(); j < jj ; j++)
        {
            kaGoalMap->_ppA[i][j] = init_ppA;
        }

    kaGoalMap->_ppA[(int)goal[0].y][(int)goal[0].x] = 0;
    kaGoalMap->_ppA[(int)goal[1].y][(int)goal[1].x] = 0;

    // 아래로 훑으면서 brushfire
    for(int i = 0 , ii = kaGoalMap->Row(); i < ii ; i++)
        for(int j = 0 , jj = kaGoalMap->Col(); j < jj ; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if(kaGoalMap->_ppA[i][j] < init_ppA)
            {
               //위
                if( i-1 >= 0 && kaGoalMap->_ppA[i-1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i-1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaGoalMap->_ppA[i+1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i + 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaGoalMap->_ppA[i][j-1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j-1] = kaGoalMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaGoalMap->_ppA[i][j+1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j+1] = kaGoalMap->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    // 위로 훑으면서 brush fire
    for(int i = kaGoalMap->Row() - 1, ii = kaGoalMap->Row(); i >= 0 ; i--)
        for(int j = kaGoalMap->Col()-1, jj = kaGoalMap->Row() ; j >= 0 ; j--)
        {
            if(kaGoalMap->_ppA[i][j] < init_ppA)
            {
               //위
                if( i-1 >= 0 && kaGoalMap->_ppA[i-1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i-1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaGoalMap->_ppA[i+1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i + 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaGoalMap->_ppA[i][j-1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j-1] = kaGoalMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaGoalMap->_ppA[i][j+1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j+1] = kaGoalMap->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

/////////////
/// \brief PotentialField::SetObstacle : kaObsMap ODE랑 똑같은 모양으로 생성
/// \param igMap
/// \param kaObsMap
///
bool PotentialField::SetObstacle(KArray<int>* kaDst)
{
    const int nObs_ppA = 0;
    const int nFreeSpace_ppA = 10000;

    for(int i = 0 , ii = kaDst->Row(); i < ii ; i++)
    {
        for(int j = 0 , jj = kaDst->Col(); j < jj ; j++)
        {
            if(150 < i && i < 350 && 150 < j && j < 250)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(150 < i &&  i < 300 && 600 < j && j < 850)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(250 < i && i < 650 && 350 < j && j < 450)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(400 < i && i < 500 && 700 < j && j < 800)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(500 < i && i < 650 && 750 < j && j < 850)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(_SQR(725-i) + _SQR(225-j) < _SQR(75))
                kaDst->_ppA[i][j] = nObs_ppA;
            else if(_SQR(775-i) + _SQR(675-j) < _SQR(75))
                kaDst->_ppA[i][j] = nObs_ppA;
            else // free space
                kaDst->_ppA[i][j] = 10000;
        }
    }

    MapIsObstacle(kaDst);
    return true;
}

///////////
/// \brief PotentialField::DrawObstacleMap : nObs_ppA와 같은 ppA이면 장애물, 아니면 free space 취급해서 그린다.
/// \param kaSrc
/// \param igDst
///
void PotentialField::DrawObstacleMap(KArray<int>* kaSrc, KImageGray* igDst)
{
    if(kaSrc->Row() == igDst->Row() && kaSrc->Col() == igDst->Col() ) // 맵 사이즈 예외처리
    {
        const int nObs_ppA = 0;
        for(int i = 0 , ii = kaSrc->Row() ; i < ii ; i ++)
        {
            for(int j = 0 , jj = kaSrc->Col(); j < jj ; j ++)
            {
                if(kaSrc->_ppA[i][j] ==nObs_ppA)
                {
                    igDst->_ppA[i][j] = 0;
                }
                else
                {
                    igDst->_ppA[i][j] = 255;
                }
            }
        }
    }
}
/////////////
/// \brief PotentialField::GetPotentialField : 이 함수 하나로 potential field의 힘과 방향을 표시한 맵 두개를 완성한다.
/// \param kaPotential_force : return 될 potential field 힘 배열
/// \param kaPotential_direction : return 될 potential 필드 방향 배열
/// \param kaObsMap :
/// \param kaGoalMap
/// \param goal
///
void PotentialField::GetPotentialField(KArray<double>* kaPotential_force,KArray<int>* kaPotential_direction,
                       KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[])
{
    BrushFire_ObstacleMap_repulsive(kaObsMap); // 장애물맵 brush fire
    BrushFire_Goal_attractive(kaGoalMap,goal); // goal맵 brush fire

    MakeRepulisveField(kaObsMap); // brush fire 한 map에서 장애물과의 거리를 참고해서 척력맵을 그린다.
    MakeAttractiveField(kaGoalMap); // brush fire한 map에서 goal과의 거리를 참고해서 인력맵을 그린다.

    MakePotentialForce(kaPotential_force); // 여기까지는 값이 제대로 들어간다.
    MakePotentialDirection(kaPotential_force,kaPotential_direction);
}


/////////////
/// \brief PotentialField::MakePotentialDirection : potential field 각 grid의 방향을 저장하는 함수
/// \param kaPF_force : force 맵의 8 neighbor를 검사하여서 나온 힘의 방향을
/// \param kaPF_direction : direction map 에 저장한다.
///
void PotentialField::MakePotentialDirection(KArray<double>* kaPF_force, KArray<int>* kaPF_direction)
{
    assert(kaPF_force->Row() == kaPF_direction->Row() && kaPF_force->Col() == kaPF_direction->Col());

    for(int i = 0 , ii = kaPF_direction->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = kaPF_direction->Col() ; j < jj ; j++)
        {
            kaPF_direction->_ppA[i][j] = GetDirection_PotentialField(kaPF_force,i,j); // 이 함수가 문제있는걸로 보인다.
        }
    }

#ifdef JDEBUG
//    for(int i = 100; i < 103 ; i++)
//    {
//        for(int j = 100 ; j < 103 ; j++)
//        {
//            qDebug() << i << j << " kaPF_force : " <<  kaPF_force->_ppA[i][j];
//        }
//    }
//    qDebug() << "kaPF_direction->_ppA[101][101]" << kaPF_direction->_ppA[101][101];

#endif
}
#define JDEBUG
/////
/// \brief PotentialField::MakePotentialForce
/// \param kaPotential_force
///
void PotentialField::MakePotentialForce(KArray<double> *kaPotential_force)
{
    assert(_kaAttractiveField.Col() == _kaRepulsiveField.Col() && _kaAttractiveField.Row() == _kaRepulsiveField.Row()
           && kaPotential_force->Row() == _kaAttractiveField.Row() && kaPotential_force->Col() == _kaAttractiveField.Col());

    for(int i = 0 , ii = kaPotential_force->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = kaPotential_force->Col() ; j < jj ; j++)
        {
            kaPotential_force->_ppA[i][j] = _kaAttractiveField._ppA[i][j] + _kaRepulsiveField._ppA[i][j];
        }
    }
#ifdef JDEBUG
    for(int j = 0 ; j < _kaRepulsiveField.Col() ; j++)
    {
        cout << "_kaAttractiveField : " <<  _kaAttractiveField._ppA[200][j] << endl;
        cout << "_kaRepulsiveField._ppA[i][j]" << _kaRepulsiveField._ppA[200][j] << endl;
        cout << "kaPotential_force 결과값 : " << kaPotential_force->_ppA[200][j] << endl;
    }
#endif

}

/////////////
/// \brief PotentialField::MakeRepulisveField   brush fire된거 참고해서 repulsive field를 각 grid에 표시하는 함수.
///                                             _kaRepulsiveField 에 표시한다.
/// \param kaObsMap : brush fire 수행되서 2차원 배열에 결과값이 적혀있는 int형 배열
///
void PotentialField::MakeRepulisveField(KArray<int>* kaObsMap)
{
    const double gridSize = 0.01;
    const double dThresDist = 70 * gridSize;
    const double K_rep = 30.0;

    _kaRepulsiveField.Create(kaObsMap->Row(), kaObsMap->Col());

    assert(_kaRepulsiveField.Row() == kaObsMap->Row() && _kaRepulsiveField.Col() == kaObsMap->Col()); // map 크기 같아야함

    // 모든 grid에 척력 저장한다.
    for(int i = 0 , ii = _kaRepulsiveField.Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = _kaRepulsiveField.Col() ; j < jj ; j++)
        {
            double dObsDist = gridSize; // kaObsMap->_ppA[i][j] == 0 일때를 대비한 초기화
            if(kaObsMap->_ppA[i][j] != 0)
            {
                dObsDist = kaObsMap->_ppA[i][j] * gridSize; // 현재 픽셀에서 장애물까지의 거리
            }


            if(dObsDist <= dThresDist) // 장애물과 거리가 일정거리 이하일때만 척력이 작용한다.
            {
                _kaRepulsiveField._ppA[i][j] = K_rep * ((1/dObsDist - 1/dThresDist) * 1/_SQR(dObsDist));
            }
            else // 장애물과 거리가 멀면 척력 작용 x
            {
                _kaRepulsiveField._ppA[i][j] = 0;
            }
        }
    }

#ifdef JDEBUG
//    for(int j = 0 ; j < _kaRepulsiveField.Col() ; j++)
//    {
//        qDebug() << "brushfire 결과값 :" << kaObsMap->_ppA[200][j];
//        qDebug() << "척력맵 결과값 : " << _kaRepulsiveField[200][j];
//    }
#endif


}

/////////////
/// \brief PotentialField::MakeAttractiveField : kaGoalMap을 참고하여 _kaAttractiveField에 인력맵을 완성한다.
/// \param kaGoalMap : brush fire를 수행한 결과값이 저장된 2차원 배열
///
void PotentialField::MakeAttractiveField(KArray<int>* kaGoalMap)
{
    const double gridSize = 0.01;
    const double K_att = 10.0;

    _kaAttractiveField.Create(kaGoalMap->Row(), kaGoalMap->Col());

    assert(_kaAttractiveField.Row() == kaGoalMap->Row() && _kaAttractiveField.Col() == kaGoalMap->Col()); // map 크기 같아야함

    // 모든 grid에 인력 저장한다.
    for(int i = 0 , ii = _kaAttractiveField.Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = _kaAttractiveField.Col() ; j < jj ; j++)
        {
            double dGoalDist = kaGoalMap->_ppA[i][j] * gridSize; // goal 까지의 거리
            _kaAttractiveField._ppA[i][j] = K_att * dGoalDist;
        }
    }

#ifdef JDEBUG
//    for(int j = 0 ; j < _kaAttractiveField.Col() ; j++)
//    {
//        qDebug() << "brushfire 결과값 :" << kaGoalMap->_ppA[200][j];
//        qDebug() << "인력맵 결과값 : " << _kaAttractiveField._ppA[200][j];
//    }
#endif

}

// 이 함수 밖으로 빼야한다.

////////////
/// \brief PotentialField::DisplayPotentialField : 포텐셜필드를 display하는건 icMap에 그려서 띄우는거다. kaPotential 을 참고해서 그린다.
/// \param icMap
/// \param kaPotentialField
///
//void PotentialField::DisplayPotentialField(KImageColor* icMap, KArray<int>* kaPotentialField)
//{
//    // icMap에 들어올때 이미 밖에서 igMap을 복사해서 왔어야 한다.

//    const int padding = 10;
//    const int interval = 5;

//    for(int i = padding, ii = kaPotentialField->Row() - padding ; i < ii ; i += interval)
//    {
//        for(int j = padding, jj = kaPotentialField->Col() - padding ; j < jj ; j+=interval)
//        {
//            int direction = GetRobotDirection_PotentialField(kaPotentialField,i,j);
//            JPOINT2 point = GetTranslationPoint(i,j,direction, 5.0);
//            //DrawLine(i,j,(int)point.x, (int)point.y, Color(), 1);  // 이 함수때문에 밖으로 나가야해


//        }
//    }
//}


JPOINT2 PotentialField::GetTranslationPoint(int i, int j, int direction, double length)
{
    double x, y;
    switch(direction)
    {
        case 0:
            x = j;
            y = i;
            return jpoint2(x,y);

        case 1:
            x = j;
            y = -length + i;
            return jpoint2(x,y);

        case 2:
            x = length * 0.7071 + j;
            y = -length * 0.7071 + i;
            return jpoint2(x,y);

        case 3:
            x = length + j;
            y = i;
            return jpoint2(x,y);
        case 4:
            x = length * 0.7071 + j;
            y = length * 0.7071 + i;
            return jpoint2(x,y);

        case 5:
            x = j;
            y = length + i;
            return jpoint2(x,y);

        case 6:
            x = -length * 0.7071 + j;
            y = length* 0.7071 + i;
            return jpoint2(x,y);

        case 7:
            x = -length + j;
            y = i;
            return jpoint2(x,y);

        case 8:
            x = -length * 0.7071 + j;
            y = -length* 0.7071 + i;
            return jpoint2(x,y);

    }
}

int PotentialField::GetDirection_PotentialField(KArray<double>* kaPF_force, int i, int j)
{
    int direction = 0;
    double dMin = kaPF_force->_ppA[i][j];

    // 8방향 검사
    // 1
    if(i-1 >= 0 && kaPF_force->_ppA[i-1][j] < dMin) // ↑ 방향 검사
    {
        direction = 1;
        dMin = kaPF_force->_ppA[i-1][j];
    }
    //2
    if(i-1 > -1 && j+1 < kaPF_force->Col() && kaPF_force->_ppA[i-1][j+1] < dMin) // ↗ 방향 검사
    {
        direction = 2;
        dMin = kaPF_force->_ppA[i-1][j+1];
    }
    //3
    if(j+1 < kaPF_force->Col() && kaPF_force->_ppA[i][j+1] < dMin) // → 방향 검사
    {
        direction = 3;
        dMin = kaPF_force->_ppA[i][j+1];
    }
    //4
    if(i+1 <kaPF_force->Row() && j+1 < kaPF_force->Col() && kaPF_force->_ppA[i+1][j+1] < dMin) // ↘ 방향 검사
    {
        direction = 4;
        dMin = kaPF_force->_ppA[i+1][j+1];
    }
    //5
    if(i+1 <kaPF_force->Row() && kaPF_force->_ppA[i+1][j] < dMin) // ↓ 방향 검사
    {
        direction = 5;
        dMin = kaPF_force->_ppA[i+1][j];
    }
    //6
    if(i+1 <kaPF_force->Row() && j-1 > -1 && kaPF_force->_ppA[i+1][j-1] < dMin) // ↙ 방향 검사
    {
        direction = 6;
        dMin = kaPF_force->_ppA[i+1][j-1];
    }
    //7
    if(j-1 > -1 && kaPF_force->_ppA[i][j-1] < dMin) // ← 방향 검사
    {
        direction = 7;
        dMin = kaPF_force->_ppA[i][j-1];
    }
    //8
    if(i-1 >= 0 && j-1 > -1 && kaPF_force->_ppA[i-1][j-1] < dMin) // ↖ 방향 검사
    {
        direction = 8;
        dMin = kaPF_force->_ppA[i-1][j-1];
    }

    return direction;

}


///////////
/// \brief PotentialField::MapIsObstacle : 맵의 경계면도 장애물로 처리한다
/// \param kaObsMap
///
void PotentialField::MapIsObstacle(KArray<int>* kaObsMap)
{
    const int nObs_ppA = 0;
    const int nLastRow = kaObsMap->Row()-1;
    for(int i = 0 , ii = kaObsMap->Col(); i < ii ; i++)
    {
        kaObsMap->_ppA[0][i] = nObs_ppA;
        kaObsMap->_ppA[nLastRow][i] = nObs_ppA;
    }

    int nLastCol = kaObsMap->Col()-1;
    for(int i = 0 , ii = kaObsMap->Row(); i< ii ; i++)
    {
        kaObsMap->_ppA[i][0] = nObs_ppA;
        kaObsMap->_ppA[i][nLastCol] = nObs_ppA;
    }
}
//------------- Likelihood field-----------------------//

void LikelihoodField::KImageGray2KArray(KImageGray* igSrc, KArray<int>* kaDst)
{
    // 이미지 사이즈 검사
    assert(igSrc->Col() == kaDst->Col() && igSrc->Row() == kaDst->Row());

    for(int i = 0 , ii = igSrc->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = igSrc->Col() ; j < jj ; j++)
        {
            kaDst->_ppA[i][j] = igSrc->_ppA[i][j];
        }
    }
}
/////////////////////
/// \brief LikelihoodField::BrushFire : 장애물에서 멀어질수록 ++ 하는식의 brush fire
/// \param kaSrc
//
void LikelihoodField::BrushFire(KArray<int>* kaSrc) // brush fire대로 픽셀 업데이트
{
    for(int i = 0 , ii = kaSrc->Row(); i < ii ; i++)
        for(int j = 0 , jj = kaSrc->Col(); j < jj ; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if(kaSrc->_ppA[i][j] < 10000)
            {
               //위
                if( i-1 >= 0 && kaSrc->_ppA[i-1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i-1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaSrc->_ppA[i+1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i + 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaSrc->_ppA[i][j-1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j-1] = kaSrc->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaSrc->_ppA[i][j+1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j+1] = kaSrc->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    for(int i = kaSrc->Row() - 1, ii = kaSrc->Row(); i >= 0 ; i--)
        for(int j = kaSrc->Col()-1, jj = kaSrc->Col() ; j >= 0 ; j--)
        {
            if(kaSrc->_ppA[i][j] < 10000)
            {
               //위
                if( i-1 >= 0 && kaSrc->_ppA[i-1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i-1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //아래
                if( i+1 <= ii - 1 && kaSrc->_ppA[i+1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i + 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //왼쪽
                if( j-1 >= 0 && kaSrc->_ppA[i][j-1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j-1] = kaSrc->_ppA[i][j] + 1;
                }
                //오른쪽
                if( j+1 <= jj-1 && kaSrc->_ppA[i][j+1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j+1] = kaSrc->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

//////
/// \brief LikelihoodField::Execute : 1. kaSrc값 참조해서 공식을 이용해서 kaLF의 각 픽셀마다 Likelihood field 값을 갱신한다.
///                                   2. LF의 최댓값, 최소값을 조사한다.(display시에 구간 나누어서 화면에 표시하기 위해)
///
void LikelihoodField::Execute(KArray<double>* kaLF, const KArray<int>* kaSrc, const double dGridSize, const double dSigmaHit,
             const double dZ_hit, const double dZ_rand, const double dZ_max)
{
    for(int i = 0 , ii = kaLF->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = kaLF->Col() ; j<jj ; j++)
        {
            double dDist = kaSrc->_ppA[i][j] * dGridSize; // 장애물로 부터 현재 픽셀까지의 거리.
            double dProb = 1 / sqrt(_2PI * _SQR(dSigmaHit)) * exp(-_SQR(dDist) / (2*_SQR(dSigmaHit)));
            kaLF->_ppA[i][j] = dZ_hit * dProb + dZ_rand / dZ_max;

            // lf 결과값중에서 최대, 최소값을 조사한다.
            if(kaLF->_ppA[i][j] > _dLfMax)
                _dLfMax = kaLF->_ppA[i][j];
            if(kaLF->_ppA[i][j] < _dLfMin)
                _dLfMin = kaLF->_ppA[i][j];
        }
    }
}
///////
/// \brief LikelihoodField::Display_LikelihoodField : execute과정에서 조사된 _dLfMax, _dLfMin값을 참고해서 화면에 밝기조절해서 표시한다.
/// \param kaSrc
/// \param igDst
///
void LikelihoodField::Display_LikelihoodField(KArray<double>* kaSrc, KImageGray* igDst)
{
    assert(kaSrc->Col() == igDst->Col() && kaSrc->Row() == igDst->Row());

    for(int i = 0 , ii = kaSrc->Row() ; i < ii ; i++)
    {
        for(int j = 0 , jj = kaSrc->Col() ; j < jj ; j++)
        {
            if(_dLfMax >= kaSrc->_ppA[i][j] && (_dLfMax * 3 + _dLfMin)/4 < kaSrc->_ppA[i][j])
            {
                igDst->_ppA[i][j] = 255;
            }
            else if( kaSrc->_ppA[i][j] > (_dLfMax + _dLfMin)/2 )
            {
                igDst->_ppA[i][j] = 170;
            }
            else if( kaSrc->_ppA[i][j] > (_dLfMax + 3*_dLfMin)/4 )
            {
                igDst->_ppA[i][j] = 85;
            }
            else if( kaSrc->_ppA[i][j] >= _dLfMin)
            {
                igDst->_ppA[i][j] = 0;
            }
        }
    }
}
