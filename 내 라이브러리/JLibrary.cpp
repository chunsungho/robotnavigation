#include "JLibrary.h"


//-------made by Jeon -------------------------------------------------------


//////////////////////////////////////////////////
/// \brief JFC::Point2Nodename
/// \param i
/// \param j
/// \param name : must be array, not pointer !!
///
void JFC::Point2Nodename(int i, int j, char* name)
{
    char tmpX[256];
    char tmpY[256];
    sprintf(tmpX, "%d", i);
    sprintf(tmpY, "%d", j);
    strcpy(name, tmpX);    strcat(name, "_");
    strcat(name, tmpY);    strcat(name, "n");
    return;
}

//////////////////////////////////////////////
// input : char형 배열을 input으로 넣어야 함
// ex input : "123_789n"
// ex output : i = 123, j = 789 // i = first num, j = second num
//////////////////////////////////////////////
position JFC::Nodename2Point(char* nodeName)
{
    position ret;
    char tmpNodename[256];
    strcpy(tmpNodename, nodeName);
    char* tmpPtr = strtok(tmpNodename, "_"); // nodeName을 '_'가 나오기 전까지 받아서 tmpPtr에 주소저장.
    char* str[2]; // str[0] : x좌표, str[1] : y좌표 저장

    str[0] = tmpPtr; // i좌표 str저장
    tmpPtr = strtok(NULL, "n");
    str[1] = tmpPtr; // j좌표 str저장

    //char* to int
    char buffer[20];
    strcpy(buffer, str[0]);  //이게 잘 안될수도 있다.
    ret.i = atoi(buffer); // first
    strcpy(buffer, str[1]);
    ret.j = atoi(buffer); // second

    return ret;
}

// 기존에 없던 id의 노드 추가
bool JFC::AddNode(const int& nX, const int& nY, const char* cID)
{
    if (FromID(cID) == nullptr)    // nullptr나오면 기존에 만들어지지 않은 id인것.
    {
        JNODE* pNode = new JNODE;
        pNode->nX = nX;
        pNode->nY = nY;
        pNode->nNeighborNum = 0;

        strcpy(pNode->cID, cID);  // 노드 이름 입력받기(copy !!)
        pNode->jpPrevious = nullptr;    // nullptr 가 아니면 closedSet에 들어간거

        vMap.push_back(pNode);
        return true;
    }
    else
    {
        return false;
    }

}

// 부모노드와 자식노드로 연결관계를 짓는 메서드
bool JFC::AddNeighbor(const char* cID, const char* cNeighbor)
{
    // 부모노드 존재여부확인 || 자식노드 존재여부확인
    if (FromID(cID) == nullptr || FromID(cNeighbor) == nullptr) // vMap must have CID node or cNeighbor node before being neighbor
    {
        return false;
    }

    JNODE* pPrevious = FromID(cID); // 부모노드
    int& nNum = pPrevious->nNeighborNum;

    for (int i = 0; i < nNum; i++)
    {
        if (strcmp(pPrevious->cNeighbors[i], cNeighbor) == 0)  // 기존에 부모노드와 자식노드 연결관계가 맺어져 있었다면
            return false;
    }

    // add
    strcpy(pPrevious->cNeighbors[nNum++], cNeighbor); // 배열에 원소 추가

    return true;
}

// JFC::vMap의 원소(포인터)가 가진 주소값을 가져오는 함수, 동적할당된 메모리의 주소를 가져온다
// 해당 id의 노드를 반환하거나 없으면 nullptr반환
jnode* JFC::FromID(const char* cID)
{
    for (int i = 0, ii = vMap.size(); i < ii; i++)
    {
        //        if(vMap[i]->cID[0] == cID[0])   // 이거 대신 strcmp 함수를 활용해보자
        //        {
        //            return vMap[i];
        //        }
        if (strcmp(vMap[i]->cID, cID) == 0) // 두 문자열이 같으면 0 출력함.
        {
            return vMap[i];
        }
    }
    return nullptr;

}
// cSrc -> cGoal 까지 직선거리로 score 계산
double JFC::Calc_score(const char* cSrc, const char* cGoal)
{
    jnode* jnSrc = FromID(cSrc);
    jnode* jnGoal = FromID(cGoal);

    if (jnSrc == nullptr || jnGoal == nullptr)
        return -1;
    int XSrc = jnSrc->nX;
    int YSrc = jnSrc->nY;
    int XGoal = jnGoal->nX;
    int YGoal = jnGoal->nY;

    double dDist = sqrt(_SQR(XSrc - XGoal) + _SQR(YSrc - YGoal));
    return dDist;
}

// cSrc에 있는 현재상태에서 cNeighbor로 가면 f_score가 얼마나 나올지 계산
double JFC::Calc_f_score(const char* cSrc, const char* cNeighbor, const char* cGoal)
{
    return Calc_score(cSrc, cNeighbor) + Calc_score(cNeighbor, cGoal); // g + h
}

const char* JFC::NodeName()
{
    return cpNodeNames[nTotalNodeCnt++];
}

void JFC::FreeMemoryfromMap()
{
    if (vMap.size() > 0)
    {
        for (int i = 0, ii = vMap.size(); i < ii; i++)
        {
            delete vMap[i];
        }
        vMap.clear();
    }
}
void JFC::DeleteOpenSet()
{
    if (size > 0)
    {
        for (int i = 0, ii = size; i < ii; i++)
        {
            Pop();
        }
    }
}

void JFC::DeletePaths()
{
    if (vPaths.size() > 0)
    {
        vPaths.clear();
    }

}

void JFC::After_A_Star_Finalize()
{
    FreeMemoryfromMap();
    DeleteOpenSet();
    DeletePaths();
}

//addNode, AddNeighbor()는 다 되어있다고 가정하고 사용한다.
// output은 vPath로 나온다.
bool JFC::A_Star(const char* cpStart, const char* cpGoal)
{
    jnode* jnSrc = FromID(cpStart);   // 출발점 알려주기, 출발점은 addNode, addNeighbor다 돼있어야해
    if (jnSrc == nullptr || FromID(cpGoal) == nullptr)
    {

        return false;
    }

    jnSrc->dCostF = Calc_f_score(cpStart, cpStart, cpGoal); // 처음 노드 f_score, g_score 계산해서 넣어야함
    jnSrc->dCostG = 0;

    Push(jnSrc);
    // 여기까지 openSet에 "A" 들어가 있어야해
    while (!Empty())
    {
        jnode* jnCur = Pop();    // vMaps의 원소가 가리키고있는 메모리의 주소를 받아옴.

        if (strcmp(jnCur->cID, cpGoal) == 0) // 현재위치가 목적지인지 검사하기
        {
            // 목적지부터 최상위 부모노드까지 출력
            while (strcmp(jnCur->cID, cpStart)) // 출발점 노드가 아니면 계속 실행
            {
                vPaths.push_back(jnCur);   // 목적지노드 -> 출발점노드전까지 모든 노드들 역순으로 저장
                jnCur = jnCur->jpPrevious;
            }
            vPaths.push_back(jnCur);  // 출발점 노드 저장
            return true;
        }

        if (jnCur->nNeighborNum > 0)    // 이웃이 하나도 없는 노드에 대한 예외처리
        {
            for (int i = 0, ii = jnCur->nNeighborNum; i < ii; i++) // 현재노드 주변 노드를 살펴보자
            {
                jnode* jnNeighbor = FromID(jnCur->cNeighbors[i]); // neighbor node
//                  qDebug() << "jnNeighbor : " << jnNeighbor->cID;
                char* cNeighbor = jnNeighbor->cID; // 문자열 o, 문자 x
                double dTentativeCostG = jnCur->dCostG + Calc_score(jnCur->cID, jnNeighbor->cID); //tentative score
                if (jnNeighbor->jpPrevious != nullptr) // neighbor가 closedSet에 포함되어 있다면
                {
                    if (dTentativeCostG >= jnNeighbor->dCostG) continue;  // 이전에 찾은 경로가 더 좋으면 continue
                }
                //if( Find(cNeighbor) == nullptr || dTentativeCostG < jnNeighbor->dCostG ) // openset에 없거나 잠정적 비용이 좋으면
                if (Top() != jnNeighbor || dTentativeCostG < jnNeighbor->dCostG) // openset에 없거나 잠정적 비용이 좋으면
                {
                    jnNeighbor->jpPrevious = jnCur; // 이전 경로 기억하기(부모노드 기억하기)
                    jnNeighbor->dCostG = dTentativeCostG; // g_score 확정
                    jnNeighbor->dCostF = jnNeighbor->dCostG + Calc_score(cNeighbor, cpGoal); // f_score 계산해서 넣기

                    if (Top() != jnNeighbor) // 근데 혹시 현재 이 neighbor가 openSet에 없었다면 push하기
                    {
                        Push(jnNeighbor);
                    }
                }
            } //for(이웃노드)
        } // if 이웃 0 개 예외처리
    } // while문

    return false;
}

// 외적의 원리를 이용한다.
int JVisibilityGraph::AreaSign(point2i A, point2i B, point2i C)
{
    double area2;
    area2 = (B.nX - A.nX) * (double)(C.nY - A.nY) - (C.nX - A.nX) * (double)(B.nY - A.nY);

    if (area2 > 0.5)    return 1;
    else if (area2 < -0.5)   return -1;
    else return 0;
}

bool JVisibilityGraph::Right(point2i A, point2i B, point2i C)
{
    return AreaSign(A, B, C) > 0;
}

bool JVisibilityGraph::Collinear(const point2i A, const point2i B, const point2i C)
{
    return (AreaSign(A, B, C) == 0 ? true : false);
}

bool JVisibilityGraph::Intersected(const point2i A, const point2i B, const point2i C, const point2i D)
{
    if (Collinear(A, B, C) || Collinear(A, B, D) || Collinear(C, D, A) || Collinear(C, D, B) || Right(A, B, C) == Right(A, B, D) ||
        Right(C, D, B) == Right(C, D, A))
        return false;

    return true;
}

bool JVisibilityGraph::isVisible(const char* cpSrc, const char* cpNeigh)
{
    JNODE* jnA = FromID(cpSrc);
    JNODE* jnB = FromID(cpNeigh);

    if (jnA != nullptr && jnB != nullptr)
    {
        //int tmpX =0, tmpY=0;
        //tmpX = jnA->nX; tmpY = jnA->nY;
        point2i A = { jnA->nX, jnA->nY };
        //tmpX = jnB->nX; tmpY = jnB->nY;
        point2i B = { jnB->nX, jnB->nY };

        for (int i = 0, ii = vObstacle.size(); i < ii; i++)
        {
            for (int j = 0, jj = vObstacle[i].size(); j < jj; j++)
            {
                point2i C = { vObstacle[i][j].first.nX, vObstacle[i][j].first.nY };
                point2i D = { vObstacle[i][(j + 1) % jj].first.nX, vObstacle[i][(j + 1) % jj].first.nY };

                if (Intersected(A, B, C, D))
                    return false;
            }
        }
        return true;    //  연결할 수 있는 상태. neighbor가 될 수 있다.
    }
}
bool JVisibilityGraph::isSameObstacle(const char* A, const char* B)
{
    int nIndexSave1 = -1, nIndexSave2 = -2; //일단 나올수 없는 인덱스로 초기화시키기
    for (int i = 0, ii = vObstacle.size(); i < ii; i++)
        for (int j = 0, jj = vObstacle[i].size(); j < jj; j++)
        {
            if (strcmp(vObstacle[i][j].second, A) == 0) // 장애물 집합에서 A와 같은 이름의 원소 발견
                nIndexSave1 = i;
            if (strcmp(vObstacle[i][j].second, B) == 0) // 장애물 집합에서 A와 같은 이름의 원소 발견
                nIndexSave2 = i;
        }

    if (nIndexSave1 == nIndexSave2)
        return true;    // 같은 장애물안에있는 꼭지점들이다.
    else
        return false;   // 같은 장애물의 꼭지점이 아니거나 장애물이 아님
}

void JVisibilityGraph::MakeObstacleNeighbor()
{
    // 같은 장애물 안에 있는 원소들을 이웃처리한다.
    for (int i = 0, ii = vObstacle.size(); i < ii; i++)
        for (int j = 0, jj = vObstacle[i].size(); j < jj; j++)
        {
            AddNeighbor(vObstacle[i][j].second, vObstacle[i][(j + 1) % jj].second);
            AddNeighbor(vObstacle[i][(j + 1) % jj].second, vObstacle[i][j].second);
        }

    // 다른 장애물에 있는 원소들을 이웃처리한다.
    char* cpNode1;
    char* cpNode2;
    for (int i = 0, ii = vMap.size(); i < ii; i++)
        for (int j = i, jj = vMap.size(); j < jj; j++)
        {
            if (i < j)   // 원소를 중복되지 않게 골라낸다
            {
                cpNode1 = vMap[i]->cID; // 노드의 이름
                cpNode2 = vMap[j]->cID; // 노드의 이름

                if (!isSameObstacle(cpNode1, cpNode2))  //다른 장애물인지 판단
                {
                    if (isVisible(cpNode1, cpNode2)) // visibility 관계에있는지 판단
                    {
                        // 다른 장애물이면서 연결할 수 있으면 서로 이웃노드로 추가한다.
                        AddNeighbor(cpNode1, cpNode2);
                        AddNeighbor(cpNode2, cpNode1);
                    }
                }
            }
        } // 이중 for문
}

//지금까지 생성된 모든 노드들에 대해 v-graph만들기 (연결관계,링크 생성)
bool JVisibilityGraph::AddNeighbor_VG()
{
    for (int i = 0, ii = vMap.size(); i < ii; i++)
    {
        for (int j = i, jj = vMap.size(); j < jj; j++)
        {
            if (i < j) // 중복으로 이웃되는거 예외처리 && 자기자신 이웃하는거 예외처리
            {
                if (isVisible(vMap[i]->cID, vMap[j]->cID)) // 노드끼리 보이면 이웃
                {
                    AddNeighbor(vMap[i]->cID, vMap[j]->cID);
                    AddNeighbor(vMap[j]->cID, vMap[i]->cID);
                }
            }
        }
    }
    return true;
}

void JVisibilityGraph::AddNode_VG(int index, int tmpX, int tmpY)
{
    pair<point2i, const char*> pair;
    const char* nodeName = NodeName();
    point2i point = { tmpX, tmpY };
    pair.first = point;
    pair.second = nodeName;
    vObstacle[index].push_back(pair);
    AddNode(tmpX, tmpY, nodeName);
}

//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::GetNearestPoint : get the nearest point from your mouse click event
/// \param igMap
/// \param point
/// \param nearestNodeName : must be array, no pointer !!!
/// \return
///
void JVoronoi::GetNearestPoint(KImageGray* igMap, char* point, char* nearestNodeName)
{
    position p = Nodename2Point(point);
    int save_i = p.i, save_j = p.j;
    int i, j;
    int upDist = 0, downDist = 0, rightDist = 0, leftDist = 0;
    int upFlag = false, downFlag = false, rightFlag = false, leftFlag = false; // exception for go out of map
    position upNearest, downNearest, rightNearest, leftNearest;

    i = save_i; j = save_j;
    // 위로 탐색
    while (i >= 0)
    {
        if (igMap->_ppA[i][j] == 255)
        {
            upFlag = true;
            upNearest.i = i;
            upNearest.j = j;
            break;
        }
        else
        {
            upDist++;
            i--;
        }
    }
    if (upFlag == false)
    {
        upDist = 99999;
    }

    i = save_i; j = save_j;
    // 아래로 탐색
    while (i < igMap->Row())
    {
        if (igMap->_ppA[i][j] == 255)
        {
            downFlag = true;
            downNearest.i = i;
            downNearest.j = j;
            break;
        }
        else
        {
            downDist++;
            i++;
        }
    }
    if (downFlag == false)
    {
        downDist = 99999;
    }

    i = save_i; j = save_j;
    // 오른쪽 탐색
    while (j < igMap->Col())
    {
        if (igMap->_ppA[i][j] == 255)
        {
            rightFlag = true;
            rightNearest.i = i;
            rightNearest.j = j;
            break;
        }
        else
        {
            rightDist++;
            j++;
        }
    }
    if (rightFlag == false)
    {
        rightDist = 99999;
    }

    i = save_i; j = save_j;
    // 왼쪽 탐색
    while (j >= 0)
    {
        if (igMap->_ppA[i][j] == 255)
        {
            leftFlag = true;
            leftNearest.i = i;
            leftNearest.j = j;
            break;
        }
        else
        {
            leftDist++;
            j--;
        }
    }
    if (leftFlag == false)
    {
        leftDist = 99999;
    }

    // compare
    if (upDist <= downDist && upDist <= leftDist && upDist <= rightDist)
    {
        Point2Nodename(upNearest.i, upNearest.j, nearestNodeName);
        return;
    }
    else if (downDist <= upDist && downDist <= rightDist && downDist <= leftDist)
    {
        Point2Nodename(downNearest.i, downNearest.j, nearestNodeName);
        return;
    }
    else if (rightDist <= upDist && rightDist <= downDist && rightDist <= leftDist)
    {
        Point2Nodename(rightNearest.i, rightNearest.j, nearestNodeName);
        return;
    }
    else
    {
        Point2Nodename(leftNearest.i, leftNearest.j, nearestNodeName);
        return;
    }
}

//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::GVD_addNeighbor : check 8-neighbor of node in JFC::vMap
/// \param igMap
///
void JVoronoi::GVD_addNeighbor(KImageGray* igMap)
{
    for (int k = 0, kk = vMap.size(); k < kk; k++)
    {
        // get point from vMap
        position cur = Nodename2Point(vMap[k]->cID);
        int i = cur.i;
        int j = cur.j;

                //i,j 하나가 주어질때
        if (i - 1 >= 0 && igMap->_ppA[i - 1][j] == 255) // ↑ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i - 1, j, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }
        if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255) // ↗ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i - 1, j + 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }
        if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255) // → 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i, j + 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }
        if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255) // ↘ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i + 1, j + 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }

        if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255) // ↓ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i + 1, j, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }

        if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255) // ↙ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i + 1, j - 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }

        if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255) // ← 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i, j - 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
//            if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }

        if (i - 1 >= 0 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255) // ↖ 방향 검사
        {
            char cNeighbor[10];
            Point2Nodename(i - 1, j - 1, cNeighbor);
            AddNeighbor(vMap[k]->cID, cNeighbor);
            //if (!AddNeighbor(vMap[k]->cID, cNeighbor)) qDebug() << "이웃 없음";
        }

    }// end of for

}

//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::GVD_addNode : node name is "i_jn" == "Y_Xn"
/// \param igMap
///
void JVoronoi::GVD_addNode(KImageGray* igMap)
{
    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            if (igMap->_ppA[i][j] == 255)
            {
                char name[10];
                Point2Nodename(i, j, name);
                AddNode(j, i, name);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::Restore : gara
/// \param igMap
///
void JVoronoi::Restore(KImageGray* igMap)
{
    for (int i = 261; i < 273; i++)
    {
        igMap->_ppA[98][i] = 255;
    }

    for (int i = 153; i < 158; i++)
    {
        igMap->_ppA[181][i] = 255;
    }
    igMap->_ppA[180][153] = 255;

    for (int i = 273; i < 289; i++)
    {
        igMap->_ppA[182][i] = 255;
    }

    igMap->_ppA[139][18] = 255;
    igMap->_ppA[139][19] = 255;
    igMap->_ppA[139][17] = 255;

    igMap->_ppA[38][53] = 255;
    igMap->_ppA[37][53] = 255;

    igMap->_ppA[110][127] = 255;
    igMap->_ppA[110][128] = 255;
}

//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::ConcatenateAllEndpoint : concatenate all endpoints to endpoints
/// \param igMap
///
bool JVoronoi::ConcatenateAllEndpoint(KImageGray* igMap)
{
    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            if (ConcatenateEndpoint(igMap, i, j))
            {
                // none
            }
            else
            {
                false;
            }
        }
    }

    return true;
}
//////////////////////////////////////////////////////////////////////
/// \brief JVoronoi::ConcatenateEndpoint : call by ConcatenateAllEndpoint method. concatenate at one point
/// \param igMap
/// \param i
/// \param j
/// \return
///
bool JVoronoi::ConcatenateEndpoint(KImageGray* igMap, int i, int j)
{
    //using isendpoint, GetNeighbor method

    if (isEndPoint(igMap, i, j))
    {
        position endPoint = { i,j };
        position neighborPos = GetNeighbor(igMap, i, j);
        if (neighborPos.i != -1) // exception
        {
            neighborPos = GetNeighbor(igMap, neighborPos, endPoint);
            if (neighborPos.i == -1)
            {
                return false; // cannot find the neighbor of endpoint's neighbor
            }
            else
            {
                position directionVector = { endPoint.i - neighborPos.i , endPoint.j - neighborPos.j };
                position tmp = endPoint;
                while (1)
                {
                    int a = directionVector.i;
                    int b = directionVector.j;
                    while (a != 0 || b != 0)
                    {
                        if (a != 0)
                        {
                            if (a > 0)
                            {
                                tmp.i++;
                                a--;
                                if (tmp.i >= igMap->Row()) // endpoint 연장하다 map 나가면
                                    return false;

                                igMap->_ppA[tmp.i][tmp.j] = 255; // 픽셀 칠하기
                                if (igMap->_ppA[tmp.i][tmp.j] == 255)
                                {
                                    // 직선잇기
                                    return true;
                                }
                            }
                            else // a < 0
                            {
                                tmp.i--;
                                a++;
                                if (tmp.i < 0) // endpoint 연장하다 map 나가면
                                    return false;

                                igMap->_ppA[tmp.i][tmp.j] = 255; // 픽셀 칠하기
                                if (igMap->_ppA[tmp.i][tmp.j] == 255) // 연장선이 다른 포인트 만날때
                                {
                                    // 직선잇기함수
                                    return true;
                                }
                            }
                        } // end of if( a != 0)

                        if (b != 0)
                        {
                            if (b > 0)
                            {
                                tmp.j++;
                                b--;
                                if (tmp.j >= igMap->Row()) // endpoint 연장하다 map 나가면
                                    return false;

                                igMap->_ppA[tmp.i][tmp.j] = 255; // 픽셀 칠하기
                                if (igMap->_ppA[tmp.i][tmp.j] == 255)
                                {
                                    return true;
                                }
                            }
                            else // b < 0
                            {
                                tmp.j--;
                                b++;
                                if (tmp.j < 0) // endpoint 연장하다 map 나가면
                                    return false;

                                igMap->_ppA[tmp.i][tmp.j] = 255; // 픽셀 칠하기
                                if (igMap->_ppA[tmp.i][tmp.j] == 255)
                                {
                                    // 직선잇기
                                    return true;
                                }
                            }
                        } // end of if ( b!= 0)
                    } // end of while

                }// end of while


                return true;
            } // end of else
        }
        else
        {
            return false; // cannot find the neighbor of endpoint
        }
    }


}
//////////////////////////////////////////////////////////
/// \brief JVoronoi::GetNeighbor : to find neighbor of endpoint's neighbor
/// \param igMap
/// \param neighbor : the neighbor of endpoint
/// \param cur : Prohibit to be same ret with cur. cur is endpoint
/// \return neighbor of endpoint's neighbor
///
position JVoronoi::GetNeighbor(KImageGray* igMap, position neighbor, position cur)
{
    int i = neighbor.i;
    int j = neighbor.j;
    position pos;
    if (i - 1 >= 0 && igMap->_ppA[i - 1][j] == 255) // ↑ 방향 검사
    {
        pos.i = i - 1; pos.j = j;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }
    if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255) // ↗ 방향 검사
    {
        pos.i = i - 1; pos.j = j + 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }
    if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255) // → 방향 검사
    {
        pos.i = i; pos.j = j + 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }
    if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255) // ↘ 방향 검사
    {
        pos.i = i + 1; pos.j = j + 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }

    if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255) // ↓ 방향 검사
    {
        pos.i = i + 1; pos.j = j;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }

    if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255) // ↙ 방향 검사
    {
        pos.i = i + 1; pos.j = j - 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }

    if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255) // ← 방향 검사
    {
        pos.i = i; pos.j = j - 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }

    if (i - 1 >= 0 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255) // ↖ 방향 검사
    {
        pos.i = i - 1; pos.j = j - 1;
        if (!(pos.i == cur.i && pos.j == cur.j)) // 같으면 안돼
            return pos;
    }
    pos.i = -1; pos.j = -1;
    return pos;
}

//////////////////////////////////////////////////////////
/// \brief JVoronoi::GetNeighbor : return neighbor when you are at endpoint, and endpoint's neighbor
/// \param igMap
/// \param i
/// \param j
/// \return position : neighbor
///         if position is (-1,-1) it means failure to find neighbor
position JVoronoi::GetNeighbor(KImageGray* igMap, int i, int j)
{
    position pos;
    if (i - 1 >= 0 && igMap->_ppA[i - 1][j] == 255) // ↑ 방향 검사
    {
        pos.i = i - 1; pos.j = j;
        return pos;
    }
    if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255) // ↗ 방향 검사
    {
        pos.i = i - 1; pos.j = j + 1;
        return pos;
    }
    if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255) // → 방향 검사
    {
        pos.i = i; pos.j = j + 1;
        return pos;
    }
    if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255) // ↘ 방향 검사
    {
        pos.i = i + 1; pos.j = j + 1;
        return pos;
    }

    if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255) // ↓ 방향 검사
    {
        pos.i = i + 1; pos.j = j;
        return pos;
    }

    if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255) // ↙ 방향 검사
    {
        pos.i = i + 1; pos.j = j - 1;
        return pos;
    }

    if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255) // ← 방향 검사
    {
        pos.i = i; pos.j = j - 1;
        return pos;
    }

    if (i - 1 >= 0 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255) // ↖ 방향 검사
    {
        pos.i = i - 1; pos.j = j - 1;
        return pos;
    }
    pos.i = -1; pos.j = -1;
    return pos;
}

//////////////////////////////////////////////////////////
/// \brief JVoronoi::isEndPoint : find the endpoint
/// \param igMap : image
/// \param i : current position row
/// \param j : current position col
/// \return true : current point is endpoint
///         false : not endpoint
bool JVoronoi::isEndPoint(KImageGray* igMap, int i, int j)
{
    if (igMap->_ppA[i][j] == 255)
    {
        int nNeighborCnt = 0;

        if (i - 1 >= 0 && igMap->_ppA[i - 1][j] == 255) // ↑ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }
        if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255) // ↗ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }
        if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255) // → 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }
        if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255) // ↘ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }

        if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255) // ↓ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }

        if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255) // ↙ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }

        if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255) // ← 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }

        if (i - 1 >= 0 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255) // ↖ 방향 검사
        {
            if (++nNeighborCnt > 1) return false;
        }

        return true;
    }
    else
    {
        return false;
    }
}

//////////////////////////////////////////////////////////
/// \brief CanErase
/// \param igMap : image
/// \param i : current position row
/// \param j : current position col
/// \return true : can erase current position pixel
///         false : cannot erase
bool JVoronoi::CanErase(KImageGray* igMap, int i, int j)
{
    if (igMap->_ppA[i][j] == 255 && i >= 0 && i < igMap->Row() && j >= 0 && j < igMap->Col()) // window중앙이 map안에 있는지 검사, 255픽셀인지 검사
    {
        if (i - 1 >= 0 && igMap->_ppA[i - 1][j] == 255) // ↑ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i - 1 > -1 && j - 1 >= 0 && igMap->_ppA[i - 1][j - 1] == 255)
                nLink++;
            if (j - 1 >= 0 && igMap->_ppA[i][j - 1] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255)
                nLink++;
            if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }
        if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255) // ↗ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i - 1 > -1 && igMap->_ppA[i - 1][j] == 255)
                nLink++;
            if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }
        if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255) // → 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i - 1 > -1 && j + 1 < igMap->Col() && igMap->_ppA[i - 1][j + 1] == 255)
                nLink++;
            if (i - 1 > -1 && igMap->_ppA[i - 1][j] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }


        if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255) // ↘ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }

        if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255) // ↓ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i + 1 < igMap->Row() && j + 1 < igMap->Col() && igMap->_ppA[i + 1][j + 1] == 255)
                nLink++;
            if (j + 1 < igMap->Col() && igMap->_ppA[i][j + 1] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255)
                nLink++;
            if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }

        if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255) // ↙ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255)
                nLink++;
            if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }

        if (j - 1 > -1 && igMap->_ppA[i][j - 1] == 255) // ← 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i + 1 < igMap->Row() && j - 1 > -1 && igMap->_ppA[i + 1][j - 1] == 255)
                nLink++;
            if (i + 1 < igMap->Row() && igMap->_ppA[i + 1][j] == 255)
                nLink++;
            if (i - 1 > -1 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255)
                nLink++;
            if (i - 1 > -1 && igMap->_ppA[i - 1][j] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }

        if (i - 1 >= 0 && j - 1 > -1 && igMap->_ppA[i - 1][j - 1] == 255) // ↖ 방향 검사
        {
            int nLink = 0;
            // neighbor의 link 개검수 사
            if (i - 1 > -1 && igMap->_ppA[i - 1][j] == 255)
                nLink++;
            if (j - 1 >= 0 && igMap->_ppA[i][j - 1] == 255)
                nLink++;

            if (nLink == 0)
            {
                return false;
            }
        }

        return true; // 여기까지 오면 주변에 약한 link가 없는거다.
    } // exception for cur position i,j
}

///////////////////////////////////////////////////////////
/// \brief JVoronoi::MakeGVDthin
/// \param igMap(보로노이맵 만들어진거)
/// igMap에 그대로 그려서 return
void JVoronoi::MakeGVDthin(KImageGray* igMap)
{
    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            if (CanErase(igMap, i, j)) // if you can erase this pixel, then erase it
            {
                igMap->_ppA[i][j] = 0;
            }
        }
    }
}

///////////////////////////////////////////////////////////
/// \brief JVoronoi::MapIsObstacle
/// \param igMap
/// 사진의 경계면,맵의 끝자리도 장애물로 취급하는 알고리즘 추가
void JVoronoi::MapIsObstacle(KImageGray* igMap)
{
    int nLastRow = igMap->Row() - 1;
    for (int i = 0, ii = igMap->Col(); i < ii; i++)
    {
        igMap->_ppA[0][i] = 0;
        igMap->_ppA[nLastRow][i] = 0;
    }

    int nLastCol = igMap->Col() - 1;
    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        igMap->_ppA[i][0] = 0;
        igMap->_ppA[i][nLastCol] = 0;
    }
#ifdef JDEBUG
    qDebug() << "MapIsObstacle 메서드 완료";
#endif
}

// ret : input으로 들어온 igMap의 픽셀을 업데이트만 하고 ret없음
void JVoronoi::BrushFire(KImageGray* igMap)
{
    MapIsObstacle(igMap);

    for (int i = 0, ii = igMap->Row(); i < ii; i++)
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if (igMap->_ppA[i][j] < 255)
            {
                //위
                if (i - 1 >= 0 && igMap->_ppA[i - 1][j] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i - 1][j] = igMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && igMap->_ppA[i + 1][j] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i + 1][j] = igMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && igMap->_ppA[i][j - 1] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i][j - 1] = igMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && igMap->_ppA[i][j + 1] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i][j + 1] = igMap->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    for (int i = igMap->Row() - 1, ii = igMap->Row(); i >= 0; i--)
        for (int j = igMap->Col() - 1, jj = igMap->Col(); j >= 0; j--)
        {
            if (igMap->_ppA[i][j] < 255)
            {
                //위
                if (i - 1 >= 0 && igMap->_ppA[i - 1][j] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i - 1][j] = igMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && igMap->_ppA[i + 1][j] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i + 1][j] = igMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && igMap->_ppA[i][j - 1] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i][j - 1] = igMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && igMap->_ppA[i][j + 1] > igMap->_ppA[i][j] + 1)
                {
                    igMap->_ppA[i][j + 1] = igMap->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

// free space의 경계면만 따서, 새로운 KImageGray로 ret
KImageGray JVoronoi::CreateVoronoiDiagram(KImageGray* igMap)
{
    BrushFire(igMap);

#ifdef JDEBUG
    qDebug() << "BrushFire 메서드 완료";
#endif

    KImageGray igVoronoi(igMap->Row(), igMap->Col()); // 맵 사이즈 지정해야함

    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            // free space는 제외
            if (igMap->_ppA[i][j] < 255)
            {
                //위
                if (i - 1 >= 0 && igMap->_ppA[i - 1][j] > igMap->_ppA[i][j])
                {
                    continue;
                }
                //아래
                else if (i + 1 <= ii - 1 && igMap->_ppA[i + 1][j] > igMap->_ppA[i][j])
                {
                    continue;
                }
                //왼쪽
                else if (j - 1 >= 0 && igMap->_ppA[i][j - 1] > igMap->_ppA[i][j])
                {
                    continue;
                }
                //오른쪽
                else if (j + 1 <= jj - 1 && igMap->_ppA[i][j + 1] > igMap->_ppA[i][j])
                {
                    continue;
                }
                // 장애물 내부는 제외
                else if (igMap->_ppA[i][j] == 0)
                {
                    continue;
                }
                else // 해당 픽셀이 voronoi의 경계면인 경우
                {
                    if (i != 0 && i != ii - 1 && j != 0 && j != jj - 1) // 맵 가장자리 제외
                    {
                        igVoronoi._ppA[i][j] = 255;
                    }

                }
            }
        }
    }

#ifdef JDEBUG
    qDebug() << "Voronoi 메서드 실행 완료";
#endif
    return igVoronoi;
}

// 인자로 받아온 이미지의 픽셀에 대해서 모든 좌표값의 크기를 2배로 높인다.
void JVoronoi::MakeDistantmapClearly(KImageGray* igMap)
{
    for (int i = 0, ii = igMap->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igMap->Col(); j < jj; j++)
        {
            if (igMap->_ppA[i][j] > 127)
            {
                igMap->_ppA[i][j] = 255;
            }
            else
            {
                igMap->_ppA[i][j] *= 5;
            }
        }
    }
}

//----------------------------------------- potential field

/////////////
/// \brief PotentialField::BrushFire_ObstacleMap_repulsive : 장애물의 위치를 받아와서 장애물 brushfire그린다.
///                                                             장애물에서 멀어질수록 값은 5씩 감소
///                                                             로봇은 에너지가 큰쪽에서 멀어지고 작은쪽으로 가려는 성질을 이용한다.
/// \param kaObsMap
///
void PotentialField::BrushFire_ObstacleMap_repulsive(KArray<int>* kaObsMap)
{
    SetObstacle(kaObsMap); // kaObs로 장애물맵을 받아온다. brushfire 하기 전 밑작업

    const int nFreeSpace_ppA = 10000;

    for (int i = 0, ii = kaObsMap->Row(); i < ii; i++)
        for (int j = 0, jj = kaObsMap->Col(); j < jj; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if (kaObsMap->_ppA[i][j] < nFreeSpace_ppA)
            {
                //위
                if (i - 1 >= 0 && kaObsMap->_ppA[i - 1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i - 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaObsMap->_ppA[i + 1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i + 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaObsMap->_ppA[i][j - 1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j - 1] = kaObsMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaObsMap->_ppA[i][j + 1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j + 1] = kaObsMap->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    for (int i = kaObsMap->Row() - 1, ii = kaObsMap->Row(); i >= 0; i--)
        for (int j = kaObsMap->Col() - 1, jj = kaObsMap->Col(); j >= 0; j--)
        {
            if (kaObsMap->_ppA[i][j] < nFreeSpace_ppA)
            {
                //위
                if (i - 1 >= 0 && kaObsMap->_ppA[i - 1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i - 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaObsMap->_ppA[i + 1][j] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i + 1][j] = kaObsMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaObsMap->_ppA[i][j - 1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j - 1] = kaObsMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaObsMap->_ppA[i][j + 1] > kaObsMap->_ppA[i][j] + 1)
                {
                    kaObsMap->_ppA[i][j + 1] = kaObsMap->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

///////////
/// \brief PotentialField::BrushFire_Goal_attractive : target 정해주면 거기로부터 멀어지면서 커져야한다.
///                                                     가장 에너지가 작은곳이 인력의 근원이 된다. 멀어질수록 kaGoalMap에 적히는 숫자는 커진다.
/// \param kaGoalMap : 이 배열에 값이 업데이트된다.
///
void PotentialField::BrushFire_Goal_attractive(KArray<int> *kaGoalMap, JPOINT2 goal[])
{
    const int init_ppA = 10000;

    // 맵 전체를 우선 init_ppA로 초기화
    for (int i = 0, ii = kaGoalMap->Row(); i < ii; i++)
        for (int j = 0, jj = kaGoalMap->Col(); j < jj; j++)
        {
            kaGoalMap->_ppA[i][j] = init_ppA;
        }

    kaGoalMap->_ppA[(int)goal[0].y][(int)goal[0].x] = 0;
    kaGoalMap->_ppA[(int)goal[1].y][(int)goal[1].x] = 0;

    // 아래로 훑으면서 brushfire
    for (int i = 0, ii = kaGoalMap->Row(); i < ii; i++)
        for (int j = 0, jj = kaGoalMap->Col(); j < jj; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if (kaGoalMap->_ppA[i][j] < init_ppA)
            {
                //위
                if (i - 1 >= 0 && kaGoalMap->_ppA[i - 1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i - 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaGoalMap->_ppA[i + 1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i + 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaGoalMap->_ppA[i][j - 1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j - 1] = kaGoalMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaGoalMap->_ppA[i][j + 1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j + 1] = kaGoalMap->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    // 위로 훑으면서 brush fire
    for (int i = kaGoalMap->Row() - 1, ii = kaGoalMap->Row(); i >= 0; i--)
        for (int j = kaGoalMap->Col() - 1, jj = kaGoalMap->Row(); j >= 0; j--)
        {
            if (kaGoalMap->_ppA[i][j] < init_ppA)
            {
                //위
                if (i - 1 >= 0 && kaGoalMap->_ppA[i - 1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i - 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaGoalMap->_ppA[i + 1][j] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i + 1][j] = kaGoalMap->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaGoalMap->_ppA[i][j - 1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j - 1] = kaGoalMap->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaGoalMap->_ppA[i][j + 1] > kaGoalMap->_ppA[i][j] + 1)
                {
                    kaGoalMap->_ppA[i][j + 1] = kaGoalMap->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

/////////////
/// \brief PotentialField::SetObstacle : kaObsMap ODE랑 똑같은 모양으로 생성
/// \param igMap
/// \param kaObsMap
///
bool PotentialField::SetObstacle(KArray<int>* kaDst)
{
    const int nObs_ppA = 0;
    const int nFreeSpace_ppA = 10000;

    for (int i = 0, ii = kaDst->Row(); i < ii; i++)
    {
        for (int j = 0, jj = kaDst->Col(); j < jj; j++)
        {
            if (150 < i && i < 350 && 150 < j && j < 250)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (150 < i &&  i < 300 && 600 < j && j < 850)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (250 < i && i < 650 && 350 < j && j < 450)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (400 < i && i < 500 && 700 < j && j < 800)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (500 < i && i < 650 && 750 < j && j < 850)
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (_SQR(725 - i) + _SQR(225 - j) < _SQR(75))
                kaDst->_ppA[i][j] = nObs_ppA;
            else if (_SQR(775 - i) + _SQR(675 - j) < _SQR(75))
                kaDst->_ppA[i][j] = nObs_ppA;
            else // free space
                kaDst->_ppA[i][j] = 10000;
        }
    }

    MapIsObstacle(kaDst);
    return true;
}

///////////
/// \brief PotentialField::DrawObstacleMap : nObs_ppA와 같은 ppA이면 장애물, 아니면 free space 취급해서 그린다.
/// \param kaSrc
/// \param igDst
///
void PotentialField::DrawObstacleMap(KArray<int>* kaSrc, KImageGray* igDst)
{
    if (kaSrc->Row() == igDst->Row() && kaSrc->Col() == igDst->Col()) // 맵 사이즈 예외처리
    {
        const int nObs_ppA = 0;
        for (int i = 0, ii = kaSrc->Row(); i < ii; i++)
        {
            for (int j = 0, jj = kaSrc->Col(); j < jj; j++)
            {
                if (kaSrc->_ppA[i][j] == nObs_ppA)
                {
                    igDst->_ppA[i][j] = 0;
                }
                else
                {
                    igDst->_ppA[i][j] = 255;
                }
            }
        }
    }
}
/////////////
/// \brief PotentialField::GetPotentialField : 이 함수 하나로 potential field의 힘과 방향을 표시한 맵 두개를 완성한다.
/// \param kaPotential_force : return 될 potential field 힘 배열
/// \param kaPotential_direction : return 될 potential 필드 방향 배열
/// \param kaObsMap :
/// \param kaGoalMap
/// \param goal
///
void PotentialField::GetPotentialField(KArray<double>* kaPotential_force, KArray<int>* kaPotential_direction,
    KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[])
{
    BrushFire_ObstacleMap_repulsive(kaObsMap); // 장애물맵 brush fire
    BrushFire_Goal_attractive(kaGoalMap, goal); // goal맵 brush fire

    MakeRepulisveField(kaObsMap); // brush fire 한 map에서 장애물과의 거리를 참고해서 척력맵을 그린다.
    MakeAttractiveField(kaGoalMap); // brush fire한 map에서 goal과의 거리를 참고해서 인력맵을 그린다.

    MakePotentialForce(kaPotential_force); // 여기까지는 값이 제대로 들어간다.
    MakePotentialDirection(kaPotential_force, kaPotential_direction);
}

// 새로
void PotentialField::GetPotentialField(KArray<double>* kaPotential_force, KArray<int>* kaPotential_direction, KArray<double> *kaEnergyField,
    KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[])
{
    BrushFire_ObstacleMap_repulsive(kaObsMap); // 장애물맵 brush fire
    BrushFire_Goal_attractive(kaGoalMap, goal); // goal맵 brush fire

    MakeRepulisveField(kaObsMap); // brush fire 한 map에서 장애물과의 거리를 참고해서 척력맵을 그린다.
    MakeAttractiveField(kaGoalMap); // brush fire한 map에서 goal과의 거리를 참고해서 인력맵을 그린다.

    MakeRepEnergy(kaObsMap);
    MakeAttEnergy(kaGoalMap);
    MakePotentEngeryField(kaEnergyField);

    MakePotentialForce(kaPotential_force); // 여기까지는 값이 제대로 들어간다.
    MakePotentialDirection(kaPotential_force, kaPotential_direction);
}
#define JDEBUG
//새로
void PotentialField::MakeRepEnergy(KArray<int>* brushFire)
{
    const double gridSize = 0.01;
    const double dThresDist = 20 * gridSize;
    const double K_rep = 10.0;

    _kaRepEnergyField.Create(brushFire->Row(), brushFire->Col());

    assert(_kaRepEnergyField.Row() == brushFire->Row() && _kaRepEnergyField.Col() == brushFire->Col()); // map 크기 같아야함

    // 모든 grid에 척력 저장한다.
    for (int i = 0, ii = _kaRepEnergyField.Row(); i < ii; i++)
    {
        for (int j = 0, jj = _kaRepEnergyField.Col(); j < jj; j++)
        {
            double dObsDist = 1. * gridSize; // kaObsMap->_ppA[i][j] == 0 일때를 대비한 초기화
            if (brushFire->_ppA[i][j] != 0)
            {
                dObsDist = brushFire->_ppA[i][j] * gridSize; // 현재 픽셀에서 장애물까지의 거리
            }


            if (dObsDist <= dThresDist) // 장애물과 거리가 일정거리 이하일때만 척력이 작용한다.
            {
                _kaRepEnergyField._ppA[i][j] = 0.5 * K_rep * _SQR(1. / dObsDist - 1. / dThresDist);
            }
            else // 장애물과 거리가 멀면 척력 작용 x
            {
                _kaRepEnergyField._ppA[i][j] = 0;
            }
        }
    }

}

// 새로
void PotentialField::MakeAttEnergy(KArray<int>* brushFire)
{
    const double K_att = 10.0;
    const double dGridSize = 0.01;

    _kaAttEnergyField.Create(brushFire->Row(), brushFire->Col());

    assert(_kaAttEnergyField.Row() == brushFire->Row() && _kaAttEnergyField.Col() == brushFire->Col()); // map 크기 같아야함

    // 모든 grid에 인력 저장한다.
    for (int i = 0, ii = _kaAttEnergyField.Row(); i < ii; i++)
    {
        for (int j = 0, jj = _kaAttEnergyField.Col(); j < jj; j++)
        {
            double dGoalDist = brushFire->_ppA[i][j] * dGridSize; // goal 까지의 거리
            _kaAttEnergyField._ppA[i][j] = 0.5 * K_att * _SQR(dGoalDist);
        }
    }


}

//새로
void PotentialField::MakePotentEngeryField(KArray<double>* energyField)
{
    assert(energyField->Row() > 0 && energyField->Col() > 0);

    for (int i = 0, ii = energyField->Row(); i < ii; i++)
    {
        for (int j = 0, jj = energyField->Col(); j < jj; j++)
        {
            energyField->_ppA[i][j] = _kaAttEnergyField._ppA[i][j] + _kaRepEnergyField._ppA[i][j];
        }
    }

}

/////////////
/// \brief PotentialField::MakePotentialDirection : potential field 각 grid의 방향을 저장하는 함수
/// \param kaPF_force : force 맵의 8 neighbor를 검사하여서 나온 힘의 방향을
/// \param kaPF_direction : direction map 에 저장한다.
///
void PotentialField::MakePotentialDirection(KArray<double>* kaPF_force, KArray<int>* kaPF_direction)
{
    assert(kaPF_force->Row() == kaPF_direction->Row() && kaPF_force->Col() == kaPF_direction->Col());

    for (int i = 0, ii = kaPF_direction->Row(); i < ii; i++)
    {
        for (int j = 0, jj = kaPF_direction->Col(); j < jj; j++)
        {
            kaPF_direction->_ppA[i][j] = GetDirection_PotentialField(kaPF_force, i, j); // 이 함수가 문제있는걸로 보인다.
        }
    }

}

/////
/// \brief PotentialField::MakePotentialForce
/// \param kaPotential_force
///
void PotentialField::MakePotentialForce(KArray<double> *kaPotential_force)
{
    assert(_kaAttractiveField.Col() == _kaRepulsiveField.Col() && _kaAttractiveField.Row() == _kaRepulsiveField.Row()
        && kaPotential_force->Row() == _kaAttractiveField.Row() && kaPotential_force->Col() == _kaAttractiveField.Col());

    for (int i = 0, ii = kaPotential_force->Row(); i < ii; i++)
    {
        for (int j = 0, jj = kaPotential_force->Col(); j < jj; j++)
        {
            kaPotential_force->_ppA[i][j] = _kaAttractiveField._ppA[i][j] + _kaRepulsiveField._ppA[i][j];
        }
    }

}

/////////////
/// \brief PotentialField::MakeRepulisveField   brush fire된거 참고해서 repulsive field를 각 grid에 표시하는 함수.
///                                             _kaRepulsiveField 에 표시한다.
/// \param kaObsMap : brush fire 수행되서 2차원 배열에 결과값이 적혀있는 int형 배열
///
void PotentialField::MakeRepulisveField(KArray<int>* kaObsMap)
{
    const double gridSize = 0.01;
    const double dThresDist = 70 * gridSize;
    const double K_rep = 30.0;

    _kaRepulsiveField.Create(kaObsMap->Row(), kaObsMap->Col());

    assert(_kaRepulsiveField.Row() == kaObsMap->Row() && _kaRepulsiveField.Col() == kaObsMap->Col()); // map 크기 같아야함

    // 모든 grid에 척력 저장한다.
    for (int i = 0, ii = _kaRepulsiveField.Row(); i < ii; i++)
    {
        for (int j = 0, jj = _kaRepulsiveField.Col(); j < jj; j++)
        {
            double dObsDist = gridSize; // kaObsMap->_ppA[i][j] == 0 일때를 대비한 초기화
            if (kaObsMap->_ppA[i][j] != 0)
            {
                dObsDist = kaObsMap->_ppA[i][j] * gridSize; // 현재 픽셀에서 장애물까지의 거리
            }


            if (dObsDist <= dThresDist) // 장애물과 거리가 일정거리 이하일때만 척력이 작용한다.
            {
                _kaRepulsiveField._ppA[i][j] = K_rep * ((1 / dObsDist - 1 / dThresDist) * 1 / _SQR(dObsDist));
            }
            else // 장애물과 거리가 멀면 척력 작용 x
            {
                _kaRepulsiveField._ppA[i][j] = 0;
            }
        }
    }


}

/////////////
/// \brief PotentialField::MakeAttractiveField : kaGoalMap을 참고하여 _kaAttractiveField에 인력맵을 완성한다.
/// \param kaGoalMap : brush fire를 수행한 결과값이 저장된 2차원 배열
///
void PotentialField::MakeAttractiveField(KArray<int>* kaGoalMap)
{
    const double gridSize = 0.01;
    const double K_att = 10.0;

    _kaAttractiveField.Create(kaGoalMap->Row(), kaGoalMap->Col());

    assert(_kaAttractiveField.Row() == kaGoalMap->Row() && _kaAttractiveField.Col() == kaGoalMap->Col()); // map 크기 같아야함

    // 모든 grid에 인력 저장한다.
    for (int i = 0, ii = _kaAttractiveField.Row(); i < ii; i++)
    {
        for (int j = 0, jj = _kaAttractiveField.Col(); j < jj; j++)
        {
            double dGoalDist = kaGoalMap->_ppA[i][j] * gridSize; // goal 까지의 거리
            _kaAttractiveField._ppA[i][j] = K_att * dGoalDist;
        }
    }

}

// 이 함수 밖으로 빼야한다.

////////////
/// \brief PotentialField::DisplayPotentialField : 포텐셜필드를 display하는건 icMap에 그려서 띄우는거다. kaPotential 을 참고해서 그린다.
/// \param icMap
/// \param kaPotentialField
///
//void PotentialField::DisplayPotentialField(KImageColor* icMap, KArray<int>* kaPotentialField)
//{
//    // icMap에 들어올때 이미 밖에서 igMap을 복사해서 왔어야 한다.

//    const int padding = 10;
//    const int interval = 5;

//    for(int i = padding, ii = kaPotentialField->Row() - padding ; i < ii ; i += interval)
//    {
//        for(int j = padding, jj = kaPotentialField->Col() - padding ; j < jj ; j+=interval)
//        {
//            int direction = GetRobotDirection_PotentialField(kaPotentialField,i,j);
//            JPOINT2 point = GetTranslationPoint(i,j,direction, 5.0);
//            //DrawLine(i,j,(int)point.x, (int)point.y, Color(), 1);  // 이 함수때문에 밖으로 나가야해


//        }
//    }
//}


JPOINT2 PotentialField::GetTranslationPoint(int i, int j, int direction, double length)
{
    double x, y;
    switch (direction)
    {
    case 0:
        x = j;
        y = i;
        return jpoint2(x, y);

    case 1:
        x = j;
        y = -length + i;
        return jpoint2(x, y);

    case 2:
        x = length * 0.7071 + j;
        y = -length * 0.7071 + i;
        return jpoint2(x, y);

    case 3:
        x = length + j;
        y = i;
        return jpoint2(x, y);
    case 4:
        x = length * 0.7071 + j;
        y = length * 0.7071 + i;
        return jpoint2(x, y);

    case 5:
        x = j;
        y = length + i;
        return jpoint2(x, y);

    case 6:
        x = -length * 0.7071 + j;
        y = length * 0.7071 + i;
        return jpoint2(x, y);

    case 7:
        x = -length + j;
        y = i;
        return jpoint2(x, y);

    case 8:
        x = -length * 0.7071 + j;
        y = -length * 0.7071 + i;
        return jpoint2(x, y);

    }
}

int PotentialField::GetDirection_PotentialField(KArray<double>* kaPF_force, int i, int j)
{
    int direction = 0;
    double dMin = kaPF_force->_ppA[i][j];

    // 8방향 검사
    // 1
    if (i - 1 >= 0 && kaPF_force->_ppA[i - 1][j] < dMin) // ↑ 방향 검사
    {
        direction = 1;
        dMin = kaPF_force->_ppA[i - 1][j];
    }
    //2
    if (i - 1 > -1 && j + 1 < kaPF_force->Col() && kaPF_force->_ppA[i - 1][j + 1] < dMin) // ↗ 방향 검사
    {
        direction = 2;
        dMin = kaPF_force->_ppA[i - 1][j + 1];
    }
    //3
    if (j + 1 < kaPF_force->Col() && kaPF_force->_ppA[i][j + 1] < dMin) // → 방향 검사
    {
        direction = 3;
        dMin = kaPF_force->_ppA[i][j + 1];
    }
    //4
    if (i + 1 < kaPF_force->Row() && j + 1 < kaPF_force->Col() && kaPF_force->_ppA[i + 1][j + 1] < dMin) // ↘ 방향 검사
    {
        direction = 4;
        dMin = kaPF_force->_ppA[i + 1][j + 1];
    }
    //5
    if (i + 1 < kaPF_force->Row() && kaPF_force->_ppA[i + 1][j] < dMin) // ↓ 방향 검사
    {
        direction = 5;
        dMin = kaPF_force->_ppA[i + 1][j];
    }
    //6
    if (i + 1 < kaPF_force->Row() && j - 1 > -1 && kaPF_force->_ppA[i + 1][j - 1] < dMin) // ↙ 방향 검사
    {
        direction = 6;
        dMin = kaPF_force->_ppA[i + 1][j - 1];
    }
    //7
    if (j - 1 > -1 && kaPF_force->_ppA[i][j - 1] < dMin) // ← 방향 검사
    {
        direction = 7;
        dMin = kaPF_force->_ppA[i][j - 1];
    }
    //8
    if (i - 1 >= 0 && j - 1 > -1 && kaPF_force->_ppA[i - 1][j - 1] < dMin) // ↖ 방향 검사
    {
        direction = 8;
        dMin = kaPF_force->_ppA[i - 1][j - 1];
    }

    return direction;

}


///////////
/// \brief PotentialField::MapIsObstacle : 맵의 경계면도 장애물로 처리한다
/// \param kaObsMap
///
void PotentialField::MapIsObstacle(KArray<int>* kaObsMap)
{
    const int nObs_ppA = 0;
    const int nLastRow = kaObsMap->Row() - 1;
    for (int i = 0, ii = kaObsMap->Col(); i < ii; i++)
    {
        kaObsMap->_ppA[0][i] = nObs_ppA;
        kaObsMap->_ppA[nLastRow][i] = nObs_ppA;
    }

    int nLastCol = kaObsMap->Col() - 1;
    for (int i = 0, ii = kaObsMap->Row(); i < ii; i++)
    {
        kaObsMap->_ppA[i][0] = nObs_ppA;
        kaObsMap->_ppA[i][nLastCol] = nObs_ppA;
    }
}
//------------- Likelihood field-----------------------//

void LikelihoodField::KImageGray2KArray(KImageGray* igSrc, KArray<int>* kaDst)
{
    // 이미지 사이즈 검사
    assert(igSrc->Col() == kaDst->Col() && igSrc->Row() == kaDst->Row());

    for (int i = 0, ii = igSrc->Row(); i < ii; i++)
    {
        for (int j = 0, jj = igSrc->Col(); j < jj; j++)
        {
            kaDst->_ppA[i][j] = igSrc->_ppA[i][j];
        }
    }
}
/////////////////////
/// \brief LikelihoodField::BrushFire : 장애물에서 멀어질수록 ++ 하는식의 brush fire
/// \param kaSrc
//
void LikelihoodField::BrushFire(KArray<int>* kaSrc) // brush fire대로 픽셀 업데이트
{
    for (int i = 0, ii = kaSrc->Row(); i < ii; i++)
        for (int j = 0, jj = kaSrc->Col(); j < jj; j++)
        {
            // 장애물이나 brush fire 체크한 부분에 대해서 적용
            if (kaSrc->_ppA[i][j] < 10000)
            {
                //위
                if (i - 1 >= 0 && kaSrc->_ppA[i - 1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i - 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaSrc->_ppA[i + 1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i + 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaSrc->_ppA[i][j - 1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j - 1] = kaSrc->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaSrc->_ppA[i][j + 1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j + 1] = kaSrc->_ppA[i][j] + 1;
                }
            }
        } // 이중for문

    for (int i = kaSrc->Row() - 1, ii = kaSrc->Row(); i >= 0; i--)
        for (int j = kaSrc->Col() - 1, jj = kaSrc->Col(); j >= 0; j--)
        {
            if (kaSrc->_ppA[i][j] < 10000)
            {
                //위
                if (i - 1 >= 0 && kaSrc->_ppA[i - 1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i - 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //아래
                if (i + 1 <= ii - 1 && kaSrc->_ppA[i + 1][j] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i + 1][j] = kaSrc->_ppA[i][j] + 1;
                }
                //왼쪽
                if (j - 1 >= 0 && kaSrc->_ppA[i][j - 1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j - 1] = kaSrc->_ppA[i][j] + 1;
                }
                //오른쪽
                if (j + 1 <= jj - 1 && kaSrc->_ppA[i][j + 1] > kaSrc->_ppA[i][j] + 1)
                {
                    kaSrc->_ppA[i][j + 1] = kaSrc->_ppA[i][j] + 1;
                }
            }
        } // 이중 for문
}

//////
/// \brief LikelihoodField::Execute : 1. kaSrc값 참조해서 공식을 이용해서 kaLF의 각 픽셀마다 Likelihood field 값을 갱신한다.
///                                   2. LF의 최댓값, 최소값을 조사한다.(display시에 구간 나누어서 화면에 표시하기 위해)
///
void LikelihoodField::Execute(KArray<double>* kaLF, const KArray<int>* kaSrc, const double dGridSize, const double dSigmaHit,
    const double dZ_hit, const double dZ_rand, const double dZ_max)
{
    for (int i = 0, ii = kaLF->Row(); i < ii; i++)
    {
        for (int j = 0, jj = kaLF->Col(); j < jj; j++)
        {
            double dDist = kaSrc->_ppA[i][j] * dGridSize; // 장애물로 부터 현재 픽셀까지의 거리.
            double dProb = 1 / sqrt(_2PI * _SQR(dSigmaHit)) * exp(-_SQR(dDist) / (2 * _SQR(dSigmaHit)));
            kaLF->_ppA[i][j] = dZ_hit * dProb + dZ_rand / dZ_max;

            // lf 결과값중에서 최대, 최소값을 조사한다.
            if (kaLF->_ppA[i][j] > _dLfMax)
                _dLfMax = kaLF->_ppA[i][j];
            if (kaLF->_ppA[i][j] < _dLfMin)
                _dLfMin = kaLF->_ppA[i][j];
        }
    }
}
///////
/// \brief LikelihoodField::Display_LikelihoodField : execute과정에서 조사된 _dLfMax, _dLfMin값을 참고해서 화면에 밝기조절해서 표시한다.
/// \param kaSrc
/// \param igDst
///
void LikelihoodField::Display_LikelihoodField(KArray<double>* kaSrc, KImageGray* igDst)
{
    assert(kaSrc->Col() == igDst->Col() && kaSrc->Row() == igDst->Row());

    for (int i = 0, ii = kaSrc->Row(); i < ii; i++)
    {
        for (int j = 0, jj = kaSrc->Col(); j < jj; j++)
        {
            if (_dLfMax >= kaSrc->_ppA[i][j] && (_dLfMax * 3 + _dLfMin) / 4 < kaSrc->_ppA[i][j])
            {
                igDst->_ppA[i][j] = 255;
            }
            else if (kaSrc->_ppA[i][j] > (_dLfMax + _dLfMin) / 2)
            {
                igDst->_ppA[i][j] = 170;
            }
            else if (kaSrc->_ppA[i][j] > (_dLfMax + 3 * _dLfMin) / 4)
            {
                igDst->_ppA[i][j] = 85;
            }
            else if (kaSrc->_ppA[i][j] >= _dLfMin)
            {
                igDst->_ppA[i][j] = 0;
            }
        }
    }
}

//--------------
PriorityQueue::PriorityQueue()
{
    size = 0;
}

PriorityQueue::~PriorityQueue()
{

}

//input : jnode*, f, g, h
bool PriorityQueue::Push(jnode* pNode)
{
    if (size >= QUEUE_MAX)   // overflow
    {
        return false;
    }
    else
    {
        //jnode* node = new jnode;   // 새 데이터를 받을 포인터 변수를 하나 만든다. 벡터는 이제 이 포인터변수를 관리하는 동적배열이 된다.
        //node = pNode;

        size++;
        pq.push_back(pNode);
        return true;
    }
}
/////////////////////////////////////////////////
/// \brief PriorityQueue::Top : Do not delete the Top node. And it is the difference with Pop()
/// \return
///
jnode* PriorityQueue::Top() // 가장 큰 값 반환
{
    if (size <= 0)
    {
        return nullptr;
    }
    else
    {
        int idx = 0;
        int min = 100000000;
        for (int i = 0, ii = pq.size(); i < ii; i++)
        {
            if (min > pq[i]->dCostF)  // 제일 작은 원소 골라
            {
                idx = i; //index 저장
                min = pq[i]->dCostF; // min 값 저장
            }
        }
        return pq[idx];
    }
}

jnode* PriorityQueue::Pop() // 가장 큰 값 반환
{
    if (size <= 0)
    {
        return nullptr;
    }
    else
    {
        int idx = 0;
        int min = 100000000;
        for (int i = 0, ii = pq.size(); i < ii; i++)
        {
            if (min > pq[i]->dCostF)  // 제일 작은 원소 골라
            {
                idx = i; //index 저장
                min = pq[i]->dCostF; // min 값 저장
            }
        }
        Swap(&pq[idx], &pq[size-1]); // vector 안의 원소를 스왑해서 가장 큰 원소를 제일 앞으로 옮긴다.(pop하기 위함)

        jnode* tmp = pq[size - 1]; // return 을 위해 잠시 저장
        pq.pop_back();
        size--; // 사이즈 줄여
        return tmp;
    }
}

void PriorityQueue::Swap(jnode** data1, jnode** data2)
{
    jnode* tmp = *data1;
    *data1 = *data2;
    *data2 = tmp;
}

int PriorityQueue::Size() // pq에 들어있는 원소 갯수 반환
{
    return size;
}

bool PriorityQueue::Empty()
{
    if (size == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

jnode* PriorityQueue::Find(char cID[10])
{
    for (int i = 0; i < size; i++)
    {
        if (pq[i]->cID[0] == cID[0])
            return pq[i];   // 있으면 주소값 반환
    }
    return nullptr; // 없으면 null
}


JOdometry::JOdometry()
{
    //robot init
    robotPosition.x = 0;
    robotPosition.y = 0;
    pre_dRobotAngle = 0;

    // sample
    gSampler.Create(0, 1);
    gSampler.OnRandom(1000);
}

JOdometry::JOdometry(int num)
{
    //robot init
    robotPosition.x = 0;
    robotPosition.y = 0;
    pre_dRobotAngle = 0;

    //sample
    gSampler.Create(0, 1);
    gSampler.OnRandom(num);
}

JOdometry::JOdometry(int num, JPOINT2 initPosition, double initRobotAngle)
{
    //robot init
    robotPosition = initPosition;
    pre_dRobotAngle = initRobotAngle;

    //sample
    gSampler.Create(0, 10);
    gSampler.OnRandom(num);
}

///////////////////////
/// \brief JOdometry::MakeGaussianNoise : make 2d noise and rotation
/// \param dAngle
/// \return
///
JPOINT2 JOdometry::MakeGaussianNoise(double dAngle) // default
{
    JPOINT2 noise(gSampler.Generate(), gSampler.Generate() * 2);
    Rotation(noise, dAngle);
    return noise;
}
////////////////////
/// \brief JOdometry::Rotation : rotation around (0,0)
/// \param src
/// \param dAngle
///
void JOdometry::Rotation(JPOINT2& src, double dAngle)
{
    dAngle = dAngle * DEG2RAD;

    double x = cos(dAngle) * src.x - sin(dAngle) * src.y;
    double y = sin(dAngle) * src.x + cos(dAngle) * src.y;

    src.x = x;
    src.y = y;
}

/////////////
/// \brief JOdometry::Rotation : rotation around org
/// \param src
/// \param org
/// \param dAngle : degree 단위로 넣어야한다.
///
void JOdometry::Rotation(JPOINT2& src, JPOINT2 org, double dAngle)
{
    //deg --> rad
    dAngle = dAngle * DEG2RAD;

    src = src - org; // translate to origin

    double x = cos(dAngle) * src.x - sin(dAngle) * src.y;
    double y = sin(dAngle) * src.x + cos(dAngle) * src.y;
    src.x = x;
    src.y = y;

    src = src + org; // move back
}
//////////////
/// \brief JOdometry::Translation translation src + x,y
/// \param src
/// \param x
/// \param y
///
void JOdometry::Translation(JPOINT2& src, int x, int y)
{
    src.x += x;
    src.y += y;
}
//////////////
/// \brief JOdometry::Translation : translation src = src + gaussian noise
/// \param src
/// \param noise
///
void JOdometry::Translation(JPOINT2& src, JPOINT2 noise)
{
    src = src + noise;
}

//////////////////
/// \brief JOdometry::DrawOdometryMap : draw vSample on igMap
/// \param igMap
/// \param vSample
///
void JOdometry::DrawOdometryMap(KImageGray* igMap, vector<JPOINT2>& vSample)
{

    for (int k = 0, kk = vSample.size(); k < kk; k++)
    {
        int j = (int)vSample[k].x;
        int i = (int)vSample[k].y;
        if (i >= 0 && i < igMap->Row() && j >= 0 && j < igMap->Col())
        {
            igMap->_ppA[i][j] = 255;
        }
    }
}
/////////////////
/// \brief JOdometry::RobotMove : sample move (rotation -> translation -> MakeGaussianNoise)
/// \param vSample
/// \param x : 로봇이 tick동안 움직인 x거리
/// \param y : 로봇이 tick동안 움직인 y거리
/// \param dAngle : 현재 로봇이 바라보는 방위
///
void JOdometry::RobotMove(vector<JPOINT2>& vSample, int x, int y, double dAngle)
{
    //회전
    double dDeltaAngle = dAngle - pre_dRobotAngle;
    for (int i = 0, ii = vSample.size(); i < ii; i++) //모든 샘플에 대해서 적용
    {
        Rotation(vSample[i], robotPosition, dDeltaAngle); // 모든 샘플을 현재 로봇 각도대로 회전
    }
    pre_dRobotAngle = dAngle; // 현재 로봇의 방위를 업데이트

    //이동
    jpoint2 trans(x, y);
    robotPosition = robotPosition + trans;
    for (int i = 0, ii = vSample.size(); i < ii; i++) // 모든 샘플에 대해서 translation
    {
        Translation(vSample[i], trans);
    }

    //노이즈
    if (firstFlag == true)
    {
        for (int i = 0, ii = vSample.size(); i < ii; i++)
        {
            vSample[i] = vSample[i] + MakeGaussianNoise(dAngle);
        }
        //        firstFlag = false;
    }

}

//△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△△ jeon sung ho
//---------------------------------------------------------------------------
