#include "JLibrary.h"

using namespace std;

int main()
{
	JFC jfc;
	jfc.AddNode(1, 3, "A");
	jfc.AddNode(1, 6, "B");
	jfc.AddNode(3, 4, "C");
	jfc.AddNode(3, 2, "D");
	jfc.AddNode(2, 1, "E");
	jfc.AddNode(3, 7, "F");
	jfc.AddNode(4, 5, "G");
	jfc.AddNode(5, 3, "H");
	jfc.AddNode(6, 6, "I");
	jfc.AddNode(7, 8, "J");
	jfc.AddNode(8, 5, "K");
	jfc.AddNode(8, 3, "L");
	jfc.AddNode(7, 1, "M");
	jfc.AddNode(9, 1, "N");
	jfc.AddNode(10, 2, "O");
	jfc.AddNode(9, 7, "P");
	jfc.AddNode(10, 5, "Q");
	jfc.AddNeighbor("A", "B");
	jfc.AddNeighbor("A", "C");
	jfc.AddNeighbor("A", "E");
	jfc.AddNeighbor("B", "F");
	jfc.AddNeighbor("C", "B");
	jfc.AddNeighbor("C", "G");
	jfc.AddNeighbor("C", "H");
	jfc.AddNeighbor("D", "C");
	jfc.AddNeighbor("D", "H");  // 유일한 목적지로의 경로
	jfc.AddNeighbor("E", "D");
	jfc.AddNeighbor("E", "M");
	jfc.AddNeighbor("F", "I");
	jfc.AddNeighbor("F", "J");
	jfc.AddNeighbor("G", "F");
	jfc.AddNeighbor("G", "I");
	jfc.AddNeighbor("H", "I");
	jfc.AddNeighbor("H", "K");
	jfc.AddNeighbor("H", "L");
	jfc.AddNeighbor("I", "K");
	jfc.AddNeighbor("I", "P");
	jfc.AddNeighbor("J", "K");
	jfc.AddNeighbor("J", "P");
	jfc.AddNeighbor("K", "P");
	jfc.AddNeighbor("K", "O");
	jfc.AddNeighbor("L", "K");
	jfc.AddNeighbor("L", "Q");
	jfc.AddNeighbor("M", "L");
	jfc.AddNeighbor("M", "N");
	jfc.AddNeighbor("N", "L");
	jfc.AddNeighbor("N", "O");
	jfc.AddNeighbor("O", "K");
	jfc.AddNeighbor("O", "Q");
	jfc.AddNeighbor("P", "K");
	jfc.AddNeighbor("P", "Q");

	if (jfc.A_Star("A", "Q")) // A* 알고리즘 적용
	{
		for (int i = 0, ii = jfc.vPaths.size(); i < ii; i++)
		{
			cout << jfc.vPaths[i]->cID << " ";
		}
	}
	else
	{
		cout << "No Path\n";
	}


	jfc.After_A_Star_Finalize();

	return 0;
}