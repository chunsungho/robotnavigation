#ifndef JLIBRARY_H
#define JLIBRARY_H

#include "kfc.h"

// A* 용
#define _MAX_NEIGHBOR_A_STAR    15
#define HEAP_MAX_SIZE           100

// 변환
#define CHAR2INT(c)             (c - '0')

// 수학계산
#define PI          3.141592653589793238462643
#define RAD2DEG     57.29577951308232087
#define DEG2RAD     0.0174532925199432957692369


// myStruct
typedef double f_score_t;

typedef struct JNODE
{
    int nX, nY;
    char cID[10];  // 자신 노드의 이름
    double dCostF, dCostG;
    char cNeighbors[_MAX_NEIGHBOR_A_STAR][11]; // 자기가 가리키는 노드의 이름
    int nNeighborNum; // 자기 자신이 가리키는 노드의 갯수
    JNODE* jpPrevious;
}jnode;

typedef struct A_STAR_NODE
{
    jnode* pNode;
    double g_score;
    double f_score;
    double h_score;
}a_node;

typedef struct POINT2I
{
    int nX;
    int nY;
}point2i;

typedef struct POSITION
{
    int i, j;
}position;

typedef struct JPOINT2
{
    double x;
    double y;
    JPOINT2() { }
    JPOINT2(double nX, double nY) : x(nX), y(nY) {  }

    JPOINT2 operator^ (double ii)
    {
        if (ii == 0.5) // sqr root
        {
            double ret_x = 2, ret_y = 2;
            for (int i = 0; i < 10; i++)
            {
                ret_x = (ret_x + (x / ret_x)) / 2;
                ret_y = (ret_y + (y / ret_y)) / 2;
            }
            return JPOINT2(ret_x, ret_y);
        }
        else if (ii > 1) // sqr
        {
            double ret_x = 1.0;
            double ret_y = 1.0;
            for (int i = 0; i < ii; i++)
            {
                ret_x *= x;
                ret_y *= y;
            }
            return JPOINT2(ret_x, ret_y);
        }
    }

    JPOINT2 operator +(JPOINT2 p1)
    {
        JPOINT2 ret;
        ret.x = p1.x + this->x;
        ret.y = p1.y + this->y;
        return ret;
    }
    JPOINT2 operator -(JPOINT2 p1)
    {
        JPOINT2 ret;
        ret.x = this->x - p1.x;
        ret.y = this->y - p1.y;
        return ret;
    }

}jpoint2;



class PriorityQueue
{
public:
    // function
    PriorityQueue();
    ~PriorityQueue();
    bool Push(jnode* pNode);
    jnode* Pop(); // 가장 작은 값 반환
    int Size(); // pq에 들어있는 원소 갯수 반환
    bool Empty();
    jnode* Top();
    jnode* Find(char cID[10]);
    int size;
    vector<jnode*> pq;
private:
    // function
    void Swap(jnode** data1, jnode** data2);
    //variables
    const int QUEUE_MAX = 9000;


protected:
};

// myClass
class JFC : public PriorityQueue {
public:
    // function
    JFC() { nTotalNodeCnt = 0; }
    ~JFC() {}

    bool AddNode(const int& nX, const int& nY, const char* cID);
    bool AddNeighbor(const char* cID, const char* cNeighbor);
    jnode* FromID(const char* cID);
    void FreeMemoryfromMap();
    void DeletePaths();
    void DeleteOpenSet();
    void After_A_Star_Finalize();
    const char* NodeName();
    void Point2Nodename(int i, int j, char* name);
    position Nodename2Point(char* nodeName);
    bool A_Star(const char* cpStart, const char* cpGoal);
    // variables
    vector<JNODE*> vMap;
    vector<JNODE*> vPaths; // AStar 메서드로 최종 생성된 경로를 저장하는 곳. 역순으로 저장된다.

private:
    //char tmp[256];
protected:
    //function
    double Calc_score(const char* cSrc, const char* cGoal);
    double Calc_f_score(const char* cSrc, const char* cNeighbor, const char* cGoal);

    //variables
    int nTotalNodeCnt = 0;
    const char* cpNodeNames[26] = { "A", "B","C","D","E","F","G","H","I","J",
                                  "K","L","M","N","O","P","Q","R","S","T",
                                  "U","V","W","X","Y","Z", };

};

class JVisibilityGraph : public JFC
{
public:
    int AreaSign(point2i A, point2i B, point2i C);
    bool Right(point2i A, point2i B, point2i C);
    bool Collinear(const point2i A, const point2i B, const point2i C);
    bool Intersected(const point2i A, const point2i B, const point2i C, const point2i D);
    bool isVisible(const char* cpSrc, const char* cpNeigh);
    bool isSameObstacle(const char* A, const char* B);
    void MakeObstacleNeighbor();
    bool AddNeighbor_VG();
    void AddNode_VG(int index, int tmpX, int tmpY);


    //variable
    vector<vector<pair<POINT2I, const char*> > > vObstacle;
private:
    const int OBSTACLE_MAX = 1000;

protected:

};

class KImageGray;

class JVoronoi : public JFC
{
public:
    void GetNearestPoint(KImageGray* igMap, char* point, char* nearestNodeName);
    void BrushFire(KImageGray* igMap); // brush fire대로 픽셀 업데이트
    KImageGray CreateVoronoiDiagram(KImageGray* igMap); // 장애물사이 경계면픽셀값을 255로 하기
    void MakeDistantmapClearly(KImageGray* igMap); // distant map으로 서서히 하얘지는 경계맵 만드는 용도
    void MakeGVDthin(KImageGray* igMap);
    bool ConcatenateEndpoint(KImageGray* igMap, int i, int j); // endpoint를 인자로 넣으면 알아서 주변 선을 잇는다.
    bool ConcatenateAllEndpoint(KImageGray* igMap);
    bool isEndPoint(KImageGray* igMap, int i, int j);
    void Restore(KImageGray* igMap);
    void GVD_addNode(KImageGray* igMap);
    void GVD_addNeighbor(KImageGray* igMap);

    //variables
    char* _cSrc;
    char* _cGoal;
private:
    void MapIsObstacle(KImageGray* igMap);
    bool CanErase(KImageGray* igMap, int i, int j);
    //    bool isEndPoint(KImageGray* igMap, int i, int j);
    position GetNeighbor(KImageGray* igMap, int i, int j);
    position GetNeighbor(KImageGray* igMap, position neighbor, position cur);

protected:

};

//class KImageColor;
//template< class T > class KArray;

class PotentialField
{
public:
    void DrawObstacleMap(KArray<int>* kaSrc, KImageGray* igDst);
    bool GetPotentialField(KArray<int>* kaPotentialField, KArray<int>* kaGoalMap, KArray<int>* kaObsMap);
    // 힘만 따진거
    void GetPotentialField(KArray<double>* kaPotential_force, KArray<int>* kaPotential_direction,
        KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[]);
    // 에너지로 따진거
    void GetPotentialField(KArray<double>* kaPotential_force, KArray<int>* kaPotential_direction,
        KArray<double>* kaEnergyField, KArray<int>* kaObsMap, KArray<int>* kaGoalMap, JPOINT2 goal[]);
    void DisplayPotentialField(KImageColor* icMap, KArray<int>* kaPotentialField);
    JPOINT2 GetTranslationPoint(int i, int j, int direction, double length);

private:
    KArray<double> _kaRepulsiveField;
    KArray<double> _kaAttractiveField;

    KArray<double> _kaAttEnergyField;
    KArray<double> _kaRepEnergyField;
private: // function
    void MakeRepEnergy(KArray<int>* brushFire);
    void MakeAttEnergy(KArray<int>* brushFire);
    void MakePotentEngeryField(KArray<double>* energyField);

    int GetDirection_PotentialField(KArray<double>* kaPF_force, int i, int j);
    void MakePotentialDirection(KArray<double>* kaPF_force, KArray<int>* kaPF_direction);
    void MakePotentialForce(KArray<double>* kaPotential_force);
    void MakeRepulisveField(KArray<int>* kaObsMap);
    void MakeAttractiveField(KArray<int>* kaGoalMap);
    void BrushFire_ObstacleMap_repulsive(KArray<int>* kaObsMap);
    void BrushFire_Goal_attractive(KArray<int> *kaGoalMap, JPOINT2 goal[]);
    void MapIsObstacle(KArray<int>* kaObsMap);
    bool SetObstacle(KArray<int>* kaDst);
    //void DrawLine(int x1, int y1, int x2, int y2);
    //QColor()
};

class LikelihoodField
{
public:
    void KImageGray2KArray(KImageGray* igSrc, KArray<int>* kaDst);
    void BrushFire(KArray<int>* kaSrc); // brush fire대로 픽셀 업데이트
    void Execute(KArray<double>* kaLF, const KArray<int>* kaSrc, const double dGridSize, const double dSigmaHit,
        const double dZ_hit, const double dZ_rand, const double dZ_max);
    void Display_LikelihoodField(KArray<double>* kaSrc, KImageGray* igDst);
private:
    double _dLfMax = -10000;
    double  _dLfMin = 10000;
protected:

};


class JOdometry
{
public:
    JOdometry();
    JOdometry(int num);
    JOdometry(int num, JPOINT2 initPosition, double initRobotAngle);
    JPOINT2 MakeGaussianNoise(double dAngle);
    void Rotation(JPOINT2& src, double dAngle); // rotation around origin
    void Rotation(JPOINT2& src, JPOINT2 org, double dAngle);
    void Translation(JPOINT2& src, int x, int y);
    void Translation(JPOINT2& src, JPOINT2 noise);

    void DrawOdometryMap(KImageGray* igMap, vector<JPOINT2>& vSample);
    void RobotMove(vector<JPOINT2>& vSample, int x, int y, double dAngle);
private:
    KGaussian gSampler;
    JPOINT2 robotPosition;
    double pre_dRobotAngle;

    bool firstFlag = true;
protected:

};


#endif JLIBRARY_H
