#ifndef MAINFRAME_H
#define MAINFRAME_H

#include <QDialog>
#include "pathplanning.h"
//#include "kfc.h"
//#include "linearTransform.h"

namespace Ui {
class MainFrame;

}

class ImageForm;
class KVoronoiGraph;
class KVisibilityGraph;
class KPotentialField;

class MainFrame : public QDialog
{
    Q_OBJECT

private:
    Ui::MainFrame *ui;

    KPtrList<ImageForm*>*   _plpImageForm;
    ImageForm*              _q_pFormFocused;

    // 전성호 선언 - 전역변수처럼 사용하기 위함
    JPOINT2 g_potential_goal[2];
    int nClickCnt = 0;
    point2i pVG_start_goal[2];

    char GVD_src[256], GVD_goal[256];
    KImageGray g_igMap;
    ImageForm*  GVD_result = 0;
    JVoronoi g_GVD;

public:
    explicit MainFrame(QWidget *parent = 0);
    ~MainFrame();

    void            ImageFormFocused(ImageForm* q_pImageForm)
                    {   _q_pFormFocused  = q_pImageForm;   //활성화된 창의 포인터를 저장함
                        UpdateUI();                        //UI 활성화 갱신
                    }
    void            UpdateUI();
    void            CloseImageForm(ImageForm* pForm);

public:
    void            OnMousePos(const int& nX, const int& nY, ImageForm* q_pForm);

private slots:
    void on_buttonOpen_clicked();
    void on_buttonDeleteContents_clicked();        
    void on_tabWidget_currentChanged(int index);           
    void on_buttonAStar_clicked();    
    void on_buttonShowList_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_VG_A_Star_clicked();

    void on_Voronoi_clicked();

    void on_test_clicked();

    void on_GVD_A_Star_clicked();

    void on_Show_Image_clicked();

    void on_odometry_clicked();

    void on_potential_fields_clicked();

    void on_load_map_clicked();

    void on_Likelihood_Field_clicked();

protected:
    void closeEvent(QCloseEvent* event);
};

#endif // MAINFRAME_H
