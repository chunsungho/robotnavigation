/****************************************************************************
** Meta object code from reading C++ file 'mainframe.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainframe.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainframe.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainFrame_t {
    QByteArrayData data[21];
    char stringdata0[429];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainFrame_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainFrame_t qt_meta_stringdata_MainFrame = {
    {
QT_MOC_LITERAL(0, 0, 9), // "MainFrame"
QT_MOC_LITERAL(1, 10, 21), // "on_buttonOpen_clicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 31), // "on_buttonDeleteContents_clicked"
QT_MOC_LITERAL(4, 65, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(5, 93, 5), // "index"
QT_MOC_LITERAL(6, 99, 22), // "on_buttonAStar_clicked"
QT_MOC_LITERAL(7, 122, 25), // "on_buttonShowList_clicked"
QT_MOC_LITERAL(8, 148, 32), // "on_horizontalSlider_valueChanged"
QT_MOC_LITERAL(9, 181, 5), // "value"
QT_MOC_LITERAL(10, 187, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(11, 209, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(12, 233, 20), // "on_VG_A_Star_clicked"
QT_MOC_LITERAL(13, 254, 18), // "on_Voronoi_clicked"
QT_MOC_LITERAL(14, 273, 15), // "on_test_clicked"
QT_MOC_LITERAL(15, 289, 21), // "on_GVD_A_Star_clicked"
QT_MOC_LITERAL(16, 311, 21), // "on_Show_Image_clicked"
QT_MOC_LITERAL(17, 333, 19), // "on_odometry_clicked"
QT_MOC_LITERAL(18, 353, 27), // "on_potential_fields_clicked"
QT_MOC_LITERAL(19, 381, 19), // "on_load_map_clicked"
QT_MOC_LITERAL(20, 401, 27) // "on_Likelihood_Field_clicked"

    },
    "MainFrame\0on_buttonOpen_clicked\0\0"
    "on_buttonDeleteContents_clicked\0"
    "on_tabWidget_currentChanged\0index\0"
    "on_buttonAStar_clicked\0on_buttonShowList_clicked\0"
    "on_horizontalSlider_valueChanged\0value\0"
    "on_pushButton_clicked\0on_pushButton_2_clicked\0"
    "on_VG_A_Star_clicked\0on_Voronoi_clicked\0"
    "on_test_clicked\0on_GVD_A_Star_clicked\0"
    "on_Show_Image_clicked\0on_odometry_clicked\0"
    "on_potential_fields_clicked\0"
    "on_load_map_clicked\0on_Likelihood_Field_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainFrame[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x08 /* Private */,
       3,    0,  100,    2, 0x08 /* Private */,
       4,    1,  101,    2, 0x08 /* Private */,
       6,    0,  104,    2, 0x08 /* Private */,
       7,    0,  105,    2, 0x08 /* Private */,
       8,    1,  106,    2, 0x08 /* Private */,
      10,    0,  109,    2, 0x08 /* Private */,
      11,    0,  110,    2, 0x08 /* Private */,
      12,    0,  111,    2, 0x08 /* Private */,
      13,    0,  112,    2, 0x08 /* Private */,
      14,    0,  113,    2, 0x08 /* Private */,
      15,    0,  114,    2, 0x08 /* Private */,
      16,    0,  115,    2, 0x08 /* Private */,
      17,    0,  116,    2, 0x08 /* Private */,
      18,    0,  117,    2, 0x08 /* Private */,
      19,    0,  118,    2, 0x08 /* Private */,
      20,    0,  119,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainFrame::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainFrame *_t = static_cast<MainFrame *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonOpen_clicked(); break;
        case 1: _t->on_buttonDeleteContents_clicked(); break;
        case 2: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_buttonAStar_clicked(); break;
        case 4: _t->on_buttonShowList_clicked(); break;
        case 5: _t->on_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_pushButton_clicked(); break;
        case 7: _t->on_pushButton_2_clicked(); break;
        case 8: _t->on_VG_A_Star_clicked(); break;
        case 9: _t->on_Voronoi_clicked(); break;
        case 10: _t->on_test_clicked(); break;
        case 11: _t->on_GVD_A_Star_clicked(); break;
        case 12: _t->on_Show_Image_clicked(); break;
        case 13: _t->on_odometry_clicked(); break;
        case 14: _t->on_potential_fields_clicked(); break;
        case 15: _t->on_load_map_clicked(); break;
        case 16: _t->on_Likelihood_Field_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainFrame::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MainFrame.data,
      qt_meta_data_MainFrame,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainFrame::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainFrame::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainFrame.stringdata0))
        return static_cast<void*>(const_cast< MainFrame*>(this));
    return QDialog::qt_metacast(_clname);
}

int MainFrame::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
