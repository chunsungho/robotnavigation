#include <QFileDialog>
#include <QPainter>

#include "mainframe.h"
#include "ui_mainframe.h"
#include "imageform.h"

MainFrame::MainFrame(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainFrame)
{
    ui->setupUi(this);

    _plpImageForm       = new KPtrList<ImageForm*>(10,false,false);
    _q_pFormFocused     = 0;

    //객체 맴버의 초기화

    //get a current directory
    char st[100];
    GetCurrentDirectoryA(100,st);

    //리스트 출력창을 안보이게    
    ui->listWidget->setVisible(false);
    this->adjustSize();

    //UI 활성화 갱신
    UpdateUI();
}

MainFrame::~MainFrame()
{ 
    delete ui;         
    delete _plpImageForm;

}

void MainFrame::CloseImageForm(ImageForm *pForm)
{
    //ImageForm 포인터 삭제
    _plpImageForm->Remove(pForm);

    //활성화 ImageForm 초기화
    _q_pFormFocused     = 0;

    //UI 활성화 갱신
    UpdateUI();
}

void MainFrame::UpdateUI()
{    
    if(ui->tabWidget->currentIndex() == 0) //rn1
    {

    }
    else if(ui->tabWidget->currentIndex() == 1) //rn2
    {

    }
}
//#define JDEBUG
void MainFrame::OnMousePos(const int &nX, const int &nY, ImageForm* q_pForm)
{
    if(q_pForm->ID() == "VG") // VG라는 윈도우를 클릭했을 때 x,y좌표가 받아와 진다.
    {
        nClickCnt++;
        if(nClickCnt == 1)
        {
            pVG_start_goal[0].nX = nX;
            pVG_start_goal[0].nY = nY;
        }
        else if(nClickCnt == 2)
        {
            pVG_start_goal[1].nX = nX;
            pVG_start_goal[1].nY = nY;
        }
        else
        {
            qDebug() << "클릭횟수 초과";
        }
    }

    if(q_pForm->ID() == "GVD") // VG라는 윈도우를 클릭했을 때 x,y좌표가 받아와 진다.
    {
        JVoronoi jvn;
        nClickCnt++;
        if(nClickCnt == 1)
        {
            jvn.Point2Nodename(nY,nX, GVD_src);
            qDebug() << GVD_src;
        }
        else if(nClickCnt == 2)
        {
            jvn.Point2Nodename(nY,nX, GVD_goal);
            qDebug() << GVD_goal;
        }
        else
        {
            qDebug() << "클릭횟수 초과";
        }
    }

    if(q_pForm->ID() == "PF_Map") // VG라는 윈도우를 클릭했을 때 x,y좌표가 받아와 진다.
    {
        nClickCnt++;
        if(nClickCnt == 1)
        {
            g_potential_goal[0].x = nX;
            g_potential_goal[0].y = nY;
        }
        else if(nClickCnt == 2)
        {
            g_potential_goal[1].x = nX;
            g_potential_goal[1].y = nY;
        }
        else
        {
            qDebug() << "클릭횟수 초과";
        }



#ifdef JDEBUG
        qDebug() << g_potential_goal.x << g_potential_goal.y;
#endif
    }


    UpdateUI();
}

void MainFrame::closeEvent(QCloseEvent* event)
{
    //생성된 ImageForm을 닫는다.
    for(int i=_plpImageForm->Count()-1; i>=0; i--)
        _plpImageForm->Item(i)->close();

    //리스트에서 삭제한다.
    _plpImageForm->RemoveAll();
}


void MainFrame::on_buttonOpen_clicked()
{
    //이미지 파일 선택
    QFileDialog::Options    q_Options   =  QFileDialog::DontResolveSymlinks  | QFileDialog::DontUseNativeDialog; // | QFileDialog::ShowDirsOnly
    QString                 q_stFile    =  QFileDialog::getOpenFileName(this, tr("Select a Image File"),  "./data", "Image Files(*.bmp *.ppm *.pgm *.png)",0, q_Options);

    if(q_stFile.length() == 0)
        return;

    //이미지 출력을 위한 ImageForm 생성    
    ImageForm*              q_pForm   = new ImageForm(q_stFile, "SOURCE", this);

    _plpImageForm->Add(q_pForm);
    q_pForm->show();

    //UI 활성화 갱신
    UpdateUI();
}

void MainFrame::on_buttonDeleteContents_clicked()
{
    //생성된 ImageForm을 닫는다.
    for(int i=_plpImageForm->Count()-1; i>=0; i--)
        _plpImageForm->Item(i)->close();

    //리스트에서 삭제한다.
    _plpImageForm->RemoveAll();
}

void MainFrame::on_tabWidget_currentChanged(int index)
{
    static int nOld = -1;

    if(nOld == 0)
    {

    }
    else if(nOld == 1)
    {

    }
    nOld = index;

    //UI 활성화 갱신
    UpdateUI();
}

void MainFrame::on_buttonShowList_clicked()
{
    static int nWidthOld = ui->tabWidget->width();

    if(ui->listWidget->isVisible())
    {
        nWidthOld = ui->listWidget->width();
        ui->listWidget->hide();
        this->adjustSize();
    }
    else
    {        
        ui->listWidget->show();
        QRect q_rcWin = this->geometry();

        this->setGeometry(q_rcWin.left(), q_rcWin.top(), q_rcWin.width()+nWidthOld, q_rcWin.height());
    }
}


void MainFrame::on_buttonAStar_clicked()
{
     QString stSource = ui->editSourceNode->text();
     QString stGoad   = ui->editGoalNode->text();
     QByteArray ba = stSource.toLocal8Bit();
    const char* cSrc = ba.data();
    ba = stGoad.toLocal8Bit();
    const char* cGoal = ba.data();

     JFC jfc;
     jfc.AddNode(1,3,"A");
     jfc.AddNode(1,6,"B");
     jfc.AddNode(3,4,"C");
     jfc.AddNode(3,2,"D");
     jfc.AddNode(2,1,"E");
     jfc.AddNode(3,7,"F");
     jfc.AddNode(4,5,"G");
     jfc.AddNode(5,3,"H");
     jfc.AddNode(6,6,"I");
     jfc.AddNode(7,8,"J");
     jfc.AddNode(8,5,"K");
     jfc.AddNode(8,3,"L");
     jfc.AddNode(7,1,"M");
     jfc.AddNode(9,1,"N");
     jfc.AddNode(10,2,"O");
     jfc.AddNode(9,7,"P");
     jfc.AddNode(10,5,"Q");
     jfc.AddNeighbor("A","B");
     jfc.AddNeighbor("A","C");
     jfc.AddNeighbor("A","E");
     jfc.AddNeighbor("B","F");
     jfc.AddNeighbor("C","B");
     jfc.AddNeighbor("C","G");
     jfc.AddNeighbor("C","H");
     jfc.AddNeighbor("D","C");
     jfc.AddNeighbor("D","H");  // 유일한 목적지로의 경로
     jfc.AddNeighbor("E","D");
     jfc.AddNeighbor("E","M");
     jfc.AddNeighbor("F","I");
     jfc.AddNeighbor("F","J");
     jfc.AddNeighbor("G","F");
     jfc.AddNeighbor("G","I");
     jfc.AddNeighbor("H","I");
     jfc.AddNeighbor("H","K");
     jfc.AddNeighbor("H","L");
     jfc.AddNeighbor("I","K");
     jfc.AddNeighbor("I","P");
     jfc.AddNeighbor("J","K");
     jfc.AddNeighbor("J","P");
     jfc.AddNeighbor("K","P");
     jfc.AddNeighbor("K","O");
     jfc.AddNeighbor("L","K");
     jfc.AddNeighbor("L","Q");
     jfc.AddNeighbor("M","L");
     jfc.AddNeighbor("M","N");
     jfc.AddNeighbor("N","L");
     jfc.AddNeighbor("N","O");
     jfc.AddNeighbor("O","K");
     jfc.AddNeighbor("O","Q");
     jfc.AddNeighbor("P","K");
     jfc.AddNeighbor("P","Q");

     if(jfc.A_Star(cSrc,cGoal)) // A* 알고리즘 적용
     {
         for(int i = 0, ii = jfc.vPaths.size(); i< ii ; i++)
         {
             ui->listWidget->insertItem(0,jfc.vPaths[i]->cID); // while문을 다 돌고 여기까지 오면 경로 못찾은경우
         }
     }
     else
     {
         ui->listWidget->insertItem(0,"No Path");
     }

     jfc.After_A_Star_Finalize();

      //UI 활성화 갱신
      UpdateUI();
}

void MainFrame::on_pushButton_clicked()
{

}

//Visibility Graph
void MainFrame::on_pushButton_2_clicked()
{
    JVisibilityGraph vg;
    QString filename = "C:/Users/JSH/Documents/01_Homeworks_RN_Student/hw02 - map.txt";
//    QString filename = "C:/Users/JSH/Documents/01_Homeworks_RN_Student/test.txt";
    QFile file(filename);

    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return;
    }
    QTextStream in(&file);
    QString myText = in.readAll();
    file.close();

    QByteArray ba = myText.toLocal8Bit();
    const char* str2char = ba.data();   // 메모장 파일 읽어오기 위함

    int nMapWidth = 0, nMapHeight = 0;
    bool bMapSizeFlag = true;
    int nSpaceFlag = 0;
    int k = -1; // vector의 row
    int tmpX = 0, tmpY = 0;
    for(int i = 0, ii = myText.length() ; i<ii ; i++)
    {
        //숫자일때
        if(str2char[i] >= '0' && str2char[i] <= '9')
        {
            //---- mapSize 파싱 ------
            if(bMapSizeFlag)
            {
                //parsing 하는 부분
                if(nSpaceFlag == 0) // mapsizeX 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    nMapWidth *= 10;
                    nMapWidth += char2int;
                }
                else if(nSpaceFlag == 1) // mapsizeY 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    nMapHeight *= 10;
                    nMapHeight += char2int;
                }
            }
            else // mapSize 파싱 끝나면 장애물 좌표 파싱
            {
                //parsing 하는 부분
                if(nSpaceFlag == 1) // tmpX 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    tmpX *= 10;
                    tmpX += char2int;
                }
                else if(nSpaceFlag == 2) // tmpY 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    tmpY *= 10;
                    tmpY += char2int;
                }
            }
        }
        // 문자일때
        else
        {
            // 개행문자 확인
            if(str2char[i] == '\n' || str2char[i+1] == '\0') // enter
            {
                if(bMapSizeFlag)
                {
                    bMapSizeFlag = false;
                }

                if(nSpaceFlag == 2)
                {
                    vg.AddNode_VG(k,tmpX,tmpY);
                }
                tmpX = 0;
                tmpY = 0;
                nSpaceFlag = 0;

                vg.vObstacle.push_back(QVector<QPair<point2i, const char*> >());
                k++;
            }
            // 스페이스 확인하는 부분
            else if(str2char[i] == ' ') // space
            {
                nSpaceFlag++;

                // vector에 push하는 부분
                if(nSpaceFlag == 3) //
                {
                    vg.AddNode_VG(k,tmpX,tmpY);

                    nSpaceFlag = 1;
                    tmpX = 0;
                    tmpY = 0;
                }
            }
        }
    } //for
    vg.AddNode_VG(k,tmpX,tmpY); /* 여기 까지 파싱한 부분 */



    //----- 이미지 그리는 창 만들기

    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "VG")
        {
            q_pForm = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(KImageColor(nMapHeight, nMapWidth), "VG", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);    //전 : 지우기도 해야되니까 일단 저장.
    }
    else
    {
        q_pForm->Update(KImageColor(nMapHeight,nMapWidth));
    }

    //------ 장애물 그리는 부분
    int nX1,nX2,nY1,nY2;
    for(int i = 0, ii = vg.vObstacle.size(); i < ii ; i++)
    {
        for(int j = 0 , jj = vg.vObstacle[i].size(); j < jj ; j ++)
        {
            nX1 = vg.vObstacle[i][j].first.nX;
            nY1 = vg.vObstacle[i][j].first.nY;

            if(j == jj-1)
            {

                nX2 = vg.vObstacle[i][0].first.nX;
                nY2 = vg.vObstacle[i][0].first.nY;
            }
            else
            {

                nX2 = vg.vObstacle[i][j+1].first.nX;
                nY2 = vg.vObstacle[i][j+1].first.nY;
            }

            q_pForm->DrawLine(nX1,nY1,nX2,nY2,QColor(0,255,255),3);
        }
    }

    vg.After_A_Star_Finalize();

    q_pForm->show();
    UpdateUI();
}

void MainFrame::on_VG_A_Star_clicked()
{
    if(nClickCnt != 2) // 예외처리 : 출발점, 목적점 둘다 정해진 경우 아니면 실행 안된다.
    {
        return;
    }

    JVisibilityGraph vg;
    QString filename = "C:/Users/JSH/Documents/01_Homeworks_RN_Student/hw02 - map.txt";
//    QString filename = "C:/Users/JSH/Documents/01_Homeworks_RN_Student/test.txt";
    QFile file(filename);

    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return;
    }
    QTextStream in(&file);
    QString myText = in.readAll();
    file.close();

    QByteArray ba = myText.toLocal8Bit();
    const char* str2char = ba.data();   // 메모장 파일 읽어오기 위함

    int nMapWidth = 0, nMapHeight = 0;
    bool bMapSizeFlag = true;
    int nSpaceFlag = 0;
    int k = -1; // vector의 row
    int tmpX = 0, tmpY = 0;
    for(int i = 0, ii = myText.length() ; i<ii ; i++)
    {
        //숫자일때
        if(str2char[i] >= '0' && str2char[i] <= '9')
        {
            //---- mapSize 파싱 ------
            if(bMapSizeFlag)
            {
                //parsing 하는 부분
                if(nSpaceFlag == 0) // mapsizeX 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    nMapWidth *= 10;
                    nMapWidth += char2int;
                }
                else if(nSpaceFlag == 1) // mapsizeY 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    nMapHeight *= 10;
                    nMapHeight += char2int;
                }
            }
            else // mapSize 파싱 끝나면 장애물 좌표 파싱
            {
                //parsing 하는 부분
                if(nSpaceFlag == 1) // tmpX 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    tmpX *= 10;
                    tmpX += char2int;
                }
                else if(nSpaceFlag == 2) // tmpY 파싱
                {
                    int char2int = CHAR2INT(str2char[i]);  // char --> int 로 먼저 변환해야한다.
                    tmpY *= 10;
                    tmpY += char2int;
                }
            }
        }
        // 문자일때
        else
        {
            // 개행문자 확인
            if(str2char[i] == '\n' || str2char[i+1] == '\0') // enter
            {
                if(bMapSizeFlag)
                {
                    bMapSizeFlag = false;
                }

                if(nSpaceFlag == 2)
                {
                    vg.AddNode_VG(k,tmpX,tmpY);
                }
                tmpX = 0;
                tmpY = 0;
                nSpaceFlag = 0;

                vg.vObstacle.push_back(QVector<QPair<point2i, const char*> >());
                k++;
            }
            // 스페이스 확인하는 부분
            else if(str2char[i] == ' ') // space
            {
                nSpaceFlag++;

                // vector에 push하는 부분
                if(nSpaceFlag == 3) //
                {
                    vg.AddNode_VG(k,tmpX,tmpY);

                    nSpaceFlag = 1;
                    tmpX = 0;
                    tmpY = 0;
                }
            }
        }
    } //for
    vg.AddNode_VG(k,tmpX,tmpY); /* 여기 까지 파싱한 부분 */

    vg.AddNode(pVG_start_goal[0].nX,pVG_start_goal[0].nY,"s"); // 출발점
    vg.AddNode(pVG_start_goal[1].nX,pVG_start_goal[1].nY,"g"); // 도착점
    vg.MakeObstacleNeighbor(); // 생성된 모든 노드에 대해 그래프를 만든다.


    //----- 이미지 그리는 창 만들기

    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "VG_A*")
        {
            q_pForm = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(KImageColor(nMapHeight, nMapWidth), "VG_A*", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);    //전 : 지우기도 해야되니까 일단 저장.
    }
    else
    {
        q_pForm->Update(KImageColor(nMapHeight,nMapWidth));
    }



    //--- A*
    if(vg.A_Star("s","g"))
    {
        for(int i = 0, ii = vg.vPaths.size(); i< ii ; i++)
        {
            ui->listWidget->insertItem(0,vg.vPaths[i]->cID); // listWidget에 최종 path 출력
        }

        //------ 장애물 그리는 부분
        int nX1,nX2,nY1,nY2;
        for(int i = 0, ii = vg.vObstacle.size(); i < ii ; i++)
        {
            for(int j = 0 , jj = vg.vObstacle[i].size(); j < jj ; j ++)
            {
                nX1 = vg.vObstacle[i][j].first.nX;
                nY1 = vg.vObstacle[i][j].first.nY;

                if(j == jj-1)
                {

                    nX2 = vg.vObstacle[i][0].first.nX;
                    nY2 = vg.vObstacle[i][0].first.nY;
                }
                else
                {

                    nX2 = vg.vObstacle[i][j+1].first.nX;
                    nY2 = vg.vObstacle[i][j+1].first.nY;
                }

                q_pForm->DrawLine(nX1,nY1,nX2,nY2,QColor(0,255,255),3);
            }
        }


      //----- A*경로 그리기
        for(int i = 0 , ii = vg.vPaths.size()-1 ; i < ii ; i++)
        {
            nX1 = vg.vPaths[i]->nX;
            nY1 = vg.vPaths[i]->nY;
            nX2 = vg.vPaths[i+1]->nX;
            nY2 = vg.vPaths[i+1]->nY;

            q_pForm->DrawLine(nX1, nY1, nX2, nY2, QColor(255,0,0),5);
        }
    }
    else
    {
        ui->listWidget->insertItem(0,"No path");
    }

    nClickCnt = 0;
    vg.After_A_Star_Finalize();

    q_pForm->show();
    UpdateUI();
}


void MainFrame::on_Voronoi_clicked()
{
    //---- 이미지 일단 받아오기
    KImageGray igSrc;
    igSrc = _q_pFormFocused->ImageGray();

    //---- brush fire수행하기
    //---- brush fire완료하면 경계선 찾기
    JVoronoi vn;
    KImageGray igMap = vn.CreateVoronoiDiagram(&igSrc); // 실선 GVD 생성

    vn.MakeGVDthin(&igMap);
    vn.Restore(&igMap);

    if(!vn.ConcatenateAllEndpoint(&igMap))
    {
        qDebug() << "Failure";
    }

    g_igMap = igMap; // save


    //----- addNode
    /*vn.GVD_addNode(&igMap);

    //------ get mouse click event point
    char* cSrc = vn.GetNearestPoint(GVD_src);
    char* cGoal = vn.GetNearestPoint(GVD_goal);

    //------- addNeighbor
    vn.GVD_addNeighbor(&igMap);*/

    //----- 이미지 창 띄우기
    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "GVD")
        {
            q_pForm = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(igMap, "GVD", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);
    }
    else
    {
        q_pForm->Update(igMap);
    }
    q_pForm->show(); // 이미지 띄우기

    UpdateUI();
}

///////////////////////////////////////////////
/// \brief MainFrame::on_GVD_A_Star_clicked : use this method after click GVD map
///
void MainFrame::on_GVD_A_Star_clicked()
{
    JVoronoi vn;
    //---- 이미지 일단 받아오기
    KImageGray igMap = g_igMap;

    //----- addNode
    vn.GVD_addNode(&igMap);

    //------ get mouse click event point
//    strcpy(GVD_src, "171_145n");
//    strcpy(GVD_goal, "178_150n");
    char cSrc[10];
    vn.GetNearestPoint(&igMap, GVD_src, cSrc);
    char cGoal[10];
    vn.GetNearestPoint(&igMap, GVD_goal, cGoal);

//    qDebug() << "다시 출력 src좌표 : " << pSrc.i << pSrc.j;
//    qDebug() << "goal좌표 : " << pGoal.i << pGoal.j;

    //------- addNeighbor
    vn.GVD_addNeighbor(&igMap);

#ifdef JDEBUG
    int nCnt[10] = {0};
    // cnt neighbor
    for(int i = 0 , ii = vn.vMap.size() ; i < ii ; i++)
    {
        nCnt[vn.vMap[i]->nNeighborNum]++;
    }

    for(int i = 0 ; i < 10 ; i++)
    {
        qDebug() << "nCnt[" << i << "] : "<< nCnt[i];
    }
#endif

    //----- 이미지 창 띄우기
    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "GVD_path")
        {
            q_pForm = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(igMap, "GVD_path", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);
    }
    else
    {
        q_pForm->Update(igMap);
    }


    //----- 이미지 창 띄우기
    ImageForm*  q_pForm1 = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "GVD_p")
        {
            q_pForm1 = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm1 == 0)
    {
        q_pForm1 = new ImageForm(KImageColor(igMap.Row(),igMap.Col()), "GVD_p", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm1);
    }
    else
    {
        q_pForm1->Update(igMap);
    }

    for(int i = 0 , ii = igMap.Row(); i < ii ; i++)
    {
        for( int j = 0 , jj = igMap.Col(); j < jj ; j++)
        {
            if(igMap._ppA[i][j] == 255)
            {
                q_pForm1->DrawLine(j, i, j, i, QColor(255,255,255),1);
            }
        }
    }
    //------- start A*
    if(vn.A_Star(cSrc,cGoal))
    {
        qDebug() << "start A*";

        //----- A*경로 그리기
        int nX1, nY1, nX2, nY2;
        for(int i = 0 , ii = vn.vPaths.size()-1 ; i < ii ; i++)
        {
            nX1 = vn.vPaths[i]->nX;
            nY1 = vn.vPaths[i]->nY;
            nX2 = vn.vPaths[i+1]->nX;
            nY2 = vn.vPaths[i+1]->nY;

            q_pForm1->DrawLine(nX1, nY1, nX2, nY2, QColor(255,0,0),1);
        }
    }


    g_GVD.vMap = vn.vMap;
    g_GVD.vPaths = vn.vPaths;
    g_GVD.pq = vn.pq;
    GVD_result = q_pForm1;
}

void MainFrame::on_Show_Image_clicked()
{
    g_GVD.After_A_Star_Finalize();
    GVD_result->show();
    UpdateUI();
}

void MainFrame::on_odometry_clicked()
{
    KImageGray igMap(700,1000);
    JOdometry odom(3000, JPOINT2(440,560), 0);
    vector<JPOINT2> vSample;

    for(int i = 0 ; i < 1000 ; i ++) // make sample at one point
    {
        vSample.push_back(JPOINT2(440, 560));
    }
    odom.DrawOdometryMap(&igMap, vSample); // 초기 맵 그리기

    odom.RobotMove(vSample, 140, 0, 0); // (580,560)
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, 140, 0, 0); // (720,560)
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, 140, 0, 0); // (860,560)
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, 0, -140, 90); // (860,420)
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, 0, -140, 90); // (860,420)
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, 0, -140, 90); // (860,420)
    odom.DrawOdometryMap(&igMap, vSample);

    //
    odom.RobotMove(vSample, -140, 0, 180);
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, -140, 0, 180);
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, -140, 0, 180);
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, -140, 0, 180);
    odom.DrawOdometryMap(&igMap, vSample);

    odom.RobotMove(vSample, -140, 0, 180);
    odom.DrawOdometryMap(&igMap, vSample);



    // draw igMap
    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
        if((*_plpImageForm)[i]->ID() == "odometry")
        {
            q_pForm = (*_plpImageForm)[i];
            break;
        }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(igMap, "odometry", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);
    }
    else
    {
        q_pForm->Update(igMap);
    }

    q_pForm->show();
    UpdateUI();
}
//#define JDEBUG

void MainFrame::on_load_map_clicked()
{
//    KImageGray igMap;
    g_igMap = _q_pFormFocused->ImageGray();

    //----------------- Display Potent field-----------------//
    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
    if((*_plpImageForm)[i]->ID() == "PF_Map")
    {
        q_pForm = (*_plpImageForm)[i];
        break;
    }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(g_igMap, "PF_Map", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);    //지우기도 해야되니까 일단 저장.
    }
    else
    {
        q_pForm->Update(g_igMap);
    }

    q_pForm->show();
    UpdateUI();
}

void MainFrame::on_potential_fields_clicked()
{

    //KImageGray igMap = g_igMap;
    const int nMapCol = 1000;
    const int nMapRow = 1000;
    KArray<int> kaObsMap(nMapRow,nMapCol);      // 척력맵 그릴때
    KArray<int> kaGoalMap(nMapRow,nMapCol);     // 인력맵 그릴때
    KArray<double> kaPotential_force(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기
    KArray<int> kaPotential_direction(nMapRow,nMapCol);      // 척력맵, 인력맵 합치기
    KArray<double> kaEnergyField(nMapRow,nMapCol);

    // goal 지정
    g_potential_goal[0].x = 150;
    g_potential_goal[0].y = 70;

    g_potential_goal[1].x = 950;
    g_potential_goal[1].y = 950;

    PotentialField pf;
    pf.GetPotentialField(&kaPotential_force,&kaPotential_direction,&kaEnergyField,&kaObsMap,&kaGoalMap,g_potential_goal); // potential Field 제작
    //pf.GetPotentialField(&kaPotential_force,&kaPotential_direction,&kaObsMap,&kaGoalMap,g_potential_goal); // potential Field 제작

    KImageGray igMap(nMapRow,nMapCol);
    pf.DrawObstacleMap(&kaObsMap, &igMap);
    KImageColor icMap = igMap.GrayToRGB();      // Display 할때 필요

    int nThres_EnergyHigh = 1800;
    int nThres_EnergyMid = 800;
    //----------------- Potential field를 Display 할 도화지 생성-----------------//
    // icMap에 들어올때 이미 밖에서 igMap을 복사해서 왔어야 한다.
    // 복사해왔으면 icMap 상태 : 장애물 -> 0,  free space -> 255

    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
    if((*_plpImageForm)[i]->ID() == "Potential Field")
    {
        q_pForm = (*_plpImageForm)[i];
        break;
    }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(icMap, "Potential Field", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);    //지우기도 해야되니까 일단 저장.
    }
    else
    {
        q_pForm->Update(icMap);
    }


    const int padding = 1;
    const int interval = 10;

    for(int i = padding, ii = kaPotential_direction.Row() - padding ; i < ii ; i += interval)   // 바꿔야
    {
        for(int j = padding, jj = kaPotential_direction.Col() - padding ; j < jj ; j+=interval)  // 바꿔야
        {
            int direction = kaPotential_direction._ppA[i][j];
            JPOINT2 point = pf.GetTranslationPoint(i,j,direction, 3.0);

            // 에너지에 따라 다른 색깔로 직선 그리기
            if(kaPotential_force._ppA[i][j] >= nThres_EnergyHigh)        // 에너지 상
            {
                q_pForm->DrawLine(j, i, (int)point.x, (int)point.y, QColor(255,0,0),1);
            }
            else if(kaPotential_force._ppA[i][j] >= nThres_EnergyMid)           // 에너지 중
            {
                q_pForm->DrawLine(j, i, (int)point.x, (int)point.y, QColor(0,0,255),1);
            }
            else                // 에너지 하
            {
                q_pForm->DrawLine(j, i, (int)point.x, (int)point.y, QColor(0,0,0),1);
            }
        }
    }

    //------ target 표시하기 -----
    const int nCheckSize = 4;
    q_pForm->DrawLine((int)g_potential_goal[0].x + nCheckSize,(int)g_potential_goal[0].y-nCheckSize,
                      (int)g_potential_goal[0].x-nCheckSize, (int)g_potential_goal[0].y+nCheckSize, QColor(255,0,0), 3);
    q_pForm->DrawLine((int)g_potential_goal[0].x + nCheckSize,(int)g_potential_goal[0].y+nCheckSize,
                      (int)g_potential_goal[0].x-nCheckSize, (int)g_potential_goal[0].y-nCheckSize, QColor(255,0,0), 3);

    q_pForm->DrawLine((int)g_potential_goal[1].x + nCheckSize,(int)g_potential_goal[1].y-nCheckSize,
                    (int)g_potential_goal[1].x-nCheckSize, (int)g_potential_goal[1].y+nCheckSize, QColor(255,0,0), 3);
    q_pForm->DrawLine((int)g_potential_goal[1].x + nCheckSize,(int)g_potential_goal[1].y+nCheckSize,
                      (int)g_potential_goal[1].x-nCheckSize, (int)g_potential_goal[1].y-nCheckSize, QColor(255,0,0), 3);



    q_pForm->show();
    UpdateUI();

    nClickCnt = 0;
}

void MainFrame::on_Likelihood_Field_clicked()
{
    KImageGray igSrc = _q_pFormFocused->ImageGray(); // free : 255, obs : 0
    KArray<int> kaSrc(igSrc.Row(), igSrc.Col()); // brush fire 할 배열

    LikelihoodField lf;
    lf.KImageGray2KArray(&igSrc, &kaSrc); // kaSrc에 이미지 픽셀값 저장하기
    lf.BrushFire(&kaSrc);

    KArray<double> kaLikelihood(kaSrc.Row(), kaSrc.Col()); // ppA 0으로 초기화

    //----------- LineEdit 에서 값 받아오는 절차 ------//
    QString stSigHit = ui->lineEdit_sigmaHit->text();
    QString stZhit   = ui->lineEdit_Zhit->text();
    QString stZrand   = ui->lineEdit_Zrand->text();
    QString stZmax   = ui->lineEdit_Zmax->text();
    QByteArray ba = stSigHit.toLocal8Bit(); // char* 로 변환을 위한 임시 변수
    const char* cSighit = ba.data();
    ba = stZhit.toLocal8Bit();
    const char* cZhit = ba.data();
    ba = stZrand.toLocal8Bit();
    const char* cZrand = ba.data();
    ba = stZmax.toLocal8Bit();
    const char* cZmax = ba.data();

    const double dGridsize = 0.05;
    const double dZ_hit = atof(cZhit);
    const double dZ_rand = atof(cZrand);
    const double dSigma_hit = atof(cSighit);
    const double dZ_max = atof(cZmax);

    // likelihood 실행
    lf.Execute(&kaLikelihood,&kaSrc, dGridsize, dSigma_hit, dZ_hit, dZ_rand, dZ_max);

#ifdef JDEBUG
    // string -> double 변환 제대로 됐는지 확인용
    qDebug() << dZ_hit << dZ_rand << dSigma_hit<< dZ_max;

    // 100 row의 픽셀값들 다 띄워보자. 대충 어떤 값들이 들어가는지 확인하게.
    for(int j = 0; j<kaLikelihood.Col(); j++)
    {

        qDebug() << "brushfire 값 : " << kaSrc[100][j] ;
        qDebug() << "Likelihood field 값" << kaLikelihood._ppA[100][j];
    }
#endif
    //------------------ 아직 다 한거 아니다. Likelihood 계산 제대로했는지 모르겠다.


    KImageGray igDisplay(kaLikelihood.Row(), kaLikelihood.Col()); // 0으로 초기화

    lf.Display_LikelihoodField(&kaLikelihood, &igDisplay);

    //----------------- Potential field를 Display 할 도화지 생성-----------------//
    ImageForm*  q_pForm = 0;
    for(int i = 0 ; i < _plpImageForm->Count(); i++)
    if((*_plpImageForm)[i]->ID() == "Likelihood Field")
    {
        q_pForm = (*_plpImageForm)[i];
        break;
    }
    if(q_pForm == 0)
    {
        q_pForm = new ImageForm(igDisplay, "Likelihood Field", this); // 이미지 그릴 도화지 생성
        _plpImageForm->Add(q_pForm);    //지우기도 해야되니까 일단 저장.
    }
    else
    {
        q_pForm->Update(igDisplay);
    }
    q_pForm->show();
    UpdateUI();


}


// 소실점 구하기
void MainFrame::on_test_clicked()
{
    // KMatrix 활용
    KMatrix T_P_W(4,4);
    T_P_W._ppA[0][0] = 0;       T_P_W._ppA[0][1] = -1;        T_P_W._ppA[0][2] = 0;       T_P_W._ppA[0][3] = 10;
    T_P_W._ppA[1][0] = -1;       T_P_W._ppA[1][1] = 0;       T_P_W._ppA[1][2] = 0;       T_P_W._ppA[1][3] = 10;
    T_P_W._ppA[2][0] = 0;       T_P_W._ppA[2][1] = 0;       T_P_W._ppA[2][2] = -1;       T_P_W._ppA[2][3] = 0;
    T_P_W._ppA[3][0] = 0;       T_P_W._ppA[3][1] = 0;       T_P_W._ppA[3][2] = 0;       T_P_W._ppA[3][3] = 1;

    KVector pxGoal(4);
    pxGoal[0] = 150 / 50.;
    pxGoal[1] = 70 / 50.;
    pxGoal[2] = 0;
    pxGoal[3] = 1;

    KVector odeGoal = T_P_W.Iv() * pxGoal;
    qDebug() << "X ,Y : " <<  odeGoal[0] << odeGoal[1];
/*
    KLinearTransform capstone;
    KMatrix intrinsicParam(3,3);

//    const double fx = 1436.35689;
//    const double fy = 1435.47675;
//    const double u0 = 951.08945;
//    const double v0 = 549.32842;

    const double fx = 948.63725;;////1436.35689;
    const double fy = 946.75161; //1435.47675;////;
    const double u0 = 622.28201; //951.08945;////;
    const double v0 = 386.01434; //549.32842;// //;

    intrinsicParam._ppA[0][0] = fx;
    intrinsicParam._ppA[1][1] = fy;
    intrinsicParam._ppA[0][2] = u0;
    intrinsicParam._ppA[1][2] = v0;
    intrinsicParam._ppA[2][2] = 1.0;
    //qDebug() << intrinsicParam[0][1];

    KPoints psImage; // px 좌표계의 점 배열

    for(int i = 0 ; i < intrinsicParam.Row(); i++)
    {
        qDebug() << intrinsicParam._ppA[i][0] << "," << intrinsicParam._ppA[i][1] << "," <<intrinsicParam._ppA[i][2] ;
    }

    psImage.Add(KPoint(613,485));   psImage.Add(KPoint(696,483));   psImage.Add(KPoint(779,482));   psImage.Add(KPoint(861,482));
    psImage.Add(KPoint(944,480));   psImage.Add(KPoint(1026,478));   psImage.Add(KPoint(1107.5,476.5));   psImage.Add(KPoint(1187.5,477.5));

    psImage.Add(KPoint(613,439));   psImage.Add(KPoint(690,438));   psImage.Add(KPoint(767,436));   psImage.Add(KPoint(844,435));
    psImage.Add(KPoint(921,434));   psImage.Add(KPoint(997.5,432.5));   psImage.Add(KPoint(1073.5,432.5));   psImage.Add(KPoint(1148.5,432));

    psImage.Add(KPoint(613,400));   psImage.Add(KPoint(685,398.5));   psImage.Add(KPoint(757,397));   psImage.Add(KPoint(829,396));
    psImage.Add(KPoint(901,394));   psImage.Add(KPoint(972.5,394));   psImage.Add(KPoint(1044,393));   psImage.Add(KPoint(1115,393));

    psImage.Add(KPoint(613,365));   psImage.Add(KPoint(680.5,364));   psImage.Add(KPoint(748.5,362.5));   psImage.Add(KPoint(816,361));
    psImage.Add(KPoint(883.5,360));   psImage.Add(KPoint(950.5,359));   psImage.Add(KPoint(1017.5,359));   psImage.Add(KPoint(1084,359));

    psImage.Add(KPoint(613,334));   psImage.Add(KPoint(677,333));   psImage.Add(KPoint(740.5,331.5));   psImage.Add(KPoint(804,331));
    psImage.Add(KPoint(867.5,329));   psImage.Add(KPoint(931.5,328));   psImage.Add(KPoint(994.5,328.5));   psImage.Add(KPoint(1058,328));

    psImage.Add(KPoint(613.5,306.5));   psImage.Add(KPoint(673.5,305 ));   psImage.Add(KPoint(733.5,304.5));   psImage.Add(KPoint(793.5,303.5));
    psImage.Add(KPoint(854,302));   psImage.Add(KPoint(913.5,301));   psImage.Add(KPoint(974,301));   psImage.Add(KPoint(1033.5,301));

//    for(int i = 0 ; i < psImage.Count(); i++)
//    {
//        psImage[i]._dX = psImage[i]._dX - psImage[0]._dX;
//        psImage[i]._dY = psImage[i]._dY - psImage[0]._dY;
//    }

    KPoints psWorld; // world 좌표계의 점 배열
    double worldX[8] = {299,494.5, 689, 884, 1079, 1273.5, 1468, 1663};
    double worldY[6] = {600, 794, 988.5, 1183.5, 1378, 1572};


    for(int i = 0 ; i < 8 ; i++)
        worldX[i] = worldX[i] - worldX[0];

    for(int i = 0 ; i < 6 ; i++)
        worldY[i] = worldY[i] - worldY[0];

    for(int yy = 0; yy < 6 ; yy++)
        for(int xx = 0 ; xx < 8 ; xx++)
        {
            psWorld.Add(KPoint(worldX[xx], worldY[yy]));  // X,Y 순서로 넣었다.
        }

//    KMatrix extrinsicParam = capstone.CameraToPlane(intrinsicParam,psImage,psWorld,true);
//    KVector vErr = capstone.HistoryErf();
//    vErr.WriteText("testError.txt");

//    qDebug() << "extrinsicParam 출력";
//    for(int i = 0 ; i < extrinsicParam.Row(); i++)
//    {
//        qDebug() << extrinsicParam[i][0] << "," << extrinsicParam[i][1] << "," <<extrinsicParam[i][2] << "," <<extrinsicParam[i][3];
//    }




//    KMatrix mTr(4,4,_IDENTITY);
//    KRotation rR;

//    rR.FromAxisRotation(_X_AXIS,_RADIAN(90.0));
//    mTr.Place(0,0,rR);
//    mTr[0][3] = 0.0; mTr[1][3] = 0.0; mTr[2][3] = 2000.0;
//    mTr.WriteText("testExtrinsic.txt");

//    psImage.RemoveAll();
//    for(int i=0; i<psWorld.Count(); i++)
//    {
//        KVector vC = mTr * KVector(psWorld[i]).Tailed(0.0).Tailed(1.0);
//        vC = intrinsicParam * vC;
//        psImage.Add(KPoint(vC[0]/vC[2], vC[1]/vC[2]));
//    }

//    KMatrix extrinsicParam = capstone.CameraToPlane(intrinsicParam,psImage,psWorld,true);
//    extrinsicParam.WriteText("testExtrinsic2.txt");


    //--------- reprojection
    KVector world(4);
    world[0] = 0;
    world[1] = 0;
    world[2] = 0;
    world[3] = 1;



    KVector(psImage[0]).WriteText("testImage.txt");

    // extrinsicParam 직접 만들기
    KMatrix extrinsicParam(4,4);
    extrinsicParam._ppA[0][0] = 0.999696;   extrinsicParam._ppA[0][1] = -0.024613;   extrinsicParam._ppA[0][2] = -0.001593;     extrinsicParam._ppA[0][3] = -0.063;
    extrinsicParam._ppA[1][0] = -0.013208;   extrinsicParam._ppA[1][1] = -0.479646;   extrinsicParam._ppA[1][2] = -0.877363;    extrinsicParam._ppA[1][3] = 0.301417;
    extrinsicParam._ppA[2][0] = 0.020830;   extrinsicParam._ppA[2][1] =0.877117;   extrinsicParam._ppA[2][2] = -0.479825;       extrinsicParam._ppA[2][3] = 2.35870;
    extrinsicParam._ppA[3][3] = 1;

    KMatrix camera =  extrinsicParam * world;

    qDebug() << "카메라좌표계 좌표";
    for(int i = 0 ; i < camera.Row(); i++)
    {
        qDebug() << camera[i][0];
    }

    KMatrix UVS = intrinsicParam * camera; // 3 X 1
    qDebug() << "u, v 출력";
    qDebug() << UVS._ppA[0][0] / UVS._ppA[2][0];
    qDebug() << UVS._ppA[1][0] / UVS._ppA[2][0];

*/
}


void MainFrame::on_horizontalSlider_valueChanged(int value)
{
    qDebug() << "helloworld";

}




