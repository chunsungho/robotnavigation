/********************************************************************************
** Form generated from reading UI file 'mainframe.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINFRAME_H
#define UI_MAINFRAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainFrame
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *buttonOpen;
    QToolButton *toolButton_2;
    QToolButton *toolButton_3;
    QToolButton *buttonDeleteContents;
    QSpacerItem *horizontalSpacer;
    QToolButton *buttonShowList;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QPushButton *buttonAStar;
    QLabel *label_17;
    QLabel *label_18;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_9;
    QLineEdit *editSourceNode;
    QLineEdit *editGoalNode;
    QPushButton *pushButton_2;
    QPushButton *VG_A_Star;
    QPushButton *Voronoi;
    QPushButton *GVD_A_Star;
    QPushButton *Show_Image;
    QWidget *tab_2;
    QPushButton *odometry;
    QSlider *horizontalSlider;
    QPushButton *potential_fields;
    QPushButton *load_map;
    QPushButton *Likelihood_Field;
    QLineEdit *lineEdit_sigmaHit;
    QLineEdit *lineEdit_Zhit;
    QLineEdit *lineEdit_Zrand;
    QLineEdit *lineEdit_Zmax;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QWidget *tab;
    QPushButton *test;
    QWidget *tab_3;
    QListWidget *listWidget;

    void setupUi(QDialog *MainFrame)
    {
        if (MainFrame->objectName().isEmpty())
            MainFrame->setObjectName(QStringLiteral("MainFrame"));
        MainFrame->resize(622, 461);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainFrame->sizePolicy().hasHeightForWidth());
        MainFrame->setSizePolicy(sizePolicy);
        MainFrame->setMinimumSize(QSize(0, 461));
        MainFrame->setModal(false);
        verticalLayout = new QVBoxLayout(MainFrame);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        frame = new QFrame(MainFrame);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(0, 41));
        frame->setMaximumSize(QSize(16777215, 41));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        buttonOpen = new QToolButton(frame);
        buttonOpen->setObjectName(QStringLiteral("buttonOpen"));
        buttonOpen->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(buttonOpen->sizePolicy().hasHeightForWidth());
        buttonOpen->setSizePolicy(sizePolicy2);
        buttonOpen->setMinimumSize(QSize(41, 41));
        buttonOpen->setMaximumSize(QSize(41, 41));
        buttonOpen->setLayoutDirection(Qt::LeftToRight);
        buttonOpen->setAutoFillBackground(false);
        QIcon icon;
        icon.addFile(QStringLiteral(":/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonOpen->setIcon(icon);
        buttonOpen->setIconSize(QSize(41, 41));
        buttonOpen->setCheckable(false);
        buttonOpen->setAutoRepeat(false);
        buttonOpen->setAutoExclusive(false);
        buttonOpen->setPopupMode(QToolButton::DelayedPopup);
        buttonOpen->setToolButtonStyle(Qt::ToolButtonIconOnly);
        buttonOpen->setAutoRaise(false);

        horizontalLayout_3->addWidget(buttonOpen);

        toolButton_2 = new QToolButton(frame);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));
        sizePolicy2.setHeightForWidth(toolButton_2->sizePolicy().hasHeightForWidth());
        toolButton_2->setSizePolicy(sizePolicy2);
        toolButton_2->setMinimumSize(QSize(41, 41));
        toolButton_2->setMaximumSize(QSize(41, 41));

        horizontalLayout_3->addWidget(toolButton_2);

        toolButton_3 = new QToolButton(frame);
        toolButton_3->setObjectName(QStringLiteral("toolButton_3"));
        sizePolicy2.setHeightForWidth(toolButton_3->sizePolicy().hasHeightForWidth());
        toolButton_3->setSizePolicy(sizePolicy2);
        toolButton_3->setMinimumSize(QSize(41, 41));
        toolButton_3->setMaximumSize(QSize(41, 41));

        horizontalLayout_3->addWidget(toolButton_3);

        buttonDeleteContents = new QToolButton(frame);
        buttonDeleteContents->setObjectName(QStringLiteral("buttonDeleteContents"));
        sizePolicy2.setHeightForWidth(buttonDeleteContents->sizePolicy().hasHeightForWidth());
        buttonDeleteContents->setSizePolicy(sizePolicy2);
        buttonDeleteContents->setMinimumSize(QSize(41, 41));
        buttonDeleteContents->setMaximumSize(QSize(41, 41));
        buttonDeleteContents->setAutoFillBackground(false);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/1-21.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonDeleteContents->setIcon(icon1);
        buttonDeleteContents->setIconSize(QSize(41, 41));

        horizontalLayout_3->addWidget(buttonDeleteContents);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        buttonShowList = new QToolButton(frame);
        buttonShowList->setObjectName(QStringLiteral("buttonShowList"));
        sizePolicy2.setHeightForWidth(buttonShowList->sizePolicy().hasHeightForWidth());
        buttonShowList->setSizePolicy(sizePolicy2);
        buttonShowList->setMinimumSize(QSize(41, 41));
        buttonShowList->setMaximumSize(QSize(41, 41));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/2-3.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonShowList->setIcon(icon2);
        buttonShowList->setIconSize(QSize(82, 41));

        horizontalLayout_3->addWidget(buttonShowList);


        verticalLayout->addWidget(frame);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(2, -1, -1, -1);
        tabWidget = new QTabWidget(MainFrame);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy3);
        tabWidget->setMinimumSize(QSize(299, 394));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        QBrush brush1(QColor(255, 85, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::NoRole, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::NoRole, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::NoRole, brush1);
        tabWidget->setPalette(palette);
        tabWidget->setCursor(QCursor(Qt::ArrowCursor));
        tabWidget->setAutoFillBackground(false);
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabWidget->setIconSize(QSize(31, 31));
        tabWidget->setElideMode(Qt::ElideMiddle);
        tab_1 = new QWidget();
        tab_1->setObjectName(QStringLiteral("tab_1"));
        buttonAStar = new QPushButton(tab_1);
        buttonAStar->setObjectName(QStringLiteral("buttonAStar"));
        buttonAStar->setGeometry(QRect(70, 58, 141, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("\353\247\221\354\235\200 \352\263\240\353\224\225"));
        font.setPointSize(10);
        buttonAStar->setFont(font);
        label_17 = new QLabel(tab_1);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(80, 21, 56, 12));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\353\247\221\354\235\200 \352\263\240\353\224\225"));
        label_17->setFont(font1);
        label_18 = new QLabel(tab_1);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(160, 21, 56, 12));
        label_18->setFont(font1);
        layoutWidget = new QWidget(tab_1);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(71, 36, 139, 24));
        horizontalLayout_9 = new QHBoxLayout(layoutWidget);
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        editSourceNode = new QLineEdit(layoutWidget);
        editSourceNode->setObjectName(QStringLiteral("editSourceNode"));
        editSourceNode->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(editSourceNode);

        editGoalNode = new QLineEdit(layoutWidget);
        editGoalNode->setObjectName(QStringLiteral("editGoalNode"));
        editGoalNode->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(editGoalNode);

        pushButton_2 = new QPushButton(tab_1);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(70, 130, 141, 31));
        VG_A_Star = new QPushButton(tab_1);
        VG_A_Star->setObjectName(QStringLiteral("VG_A_Star"));
        VG_A_Star->setGeometry(QRect(70, 160, 141, 31));
        Voronoi = new QPushButton(tab_1);
        Voronoi->setObjectName(QStringLiteral("Voronoi"));
        Voronoi->setGeometry(QRect(70, 250, 141, 31));
        GVD_A_Star = new QPushButton(tab_1);
        GVD_A_Star->setObjectName(QStringLiteral("GVD_A_Star"));
        GVD_A_Star->setGeometry(QRect(70, 280, 141, 31));
        Show_Image = new QPushButton(tab_1);
        Show_Image->setObjectName(QStringLiteral("Show_Image"));
        Show_Image->setGeometry(QRect(70, 310, 141, 31));
        tabWidget->addTab(tab_1, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        odometry = new QPushButton(tab_2);
        odometry->setObjectName(QStringLiteral("odometry"));
        odometry->setGeometry(QRect(40, 20, 201, 41));
        horizontalSlider = new QSlider(tab_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(0, 340, 160, 16));
        horizontalSlider->setOrientation(Qt::Horizontal);
        potential_fields = new QPushButton(tab_2);
        potential_fields->setObjectName(QStringLiteral("potential_fields"));
        potential_fields->setGeometry(QRect(40, 110, 201, 31));
        load_map = new QPushButton(tab_2);
        load_map->setObjectName(QStringLiteral("load_map"));
        load_map->setGeometry(QRect(40, 90, 201, 21));
        Likelihood_Field = new QPushButton(tab_2);
        Likelihood_Field->setObjectName(QStringLiteral("Likelihood_Field"));
        Likelihood_Field->setGeometry(QRect(40, 250, 201, 31));
        lineEdit_sigmaHit = new QLineEdit(tab_2);
        lineEdit_sigmaHit->setObjectName(QStringLiteral("lineEdit_sigmaHit"));
        lineEdit_sigmaHit->setGeometry(QRect(40, 170, 51, 21));
        lineEdit_Zhit = new QLineEdit(tab_2);
        lineEdit_Zhit->setObjectName(QStringLiteral("lineEdit_Zhit"));
        lineEdit_Zhit->setGeometry(QRect(160, 170, 71, 21));
        lineEdit_Zrand = new QLineEdit(tab_2);
        lineEdit_Zrand->setObjectName(QStringLiteral("lineEdit_Zrand"));
        lineEdit_Zrand->setGeometry(QRect(40, 220, 71, 21));
        lineEdit_Zmax = new QLineEdit(tab_2);
        lineEdit_Zmax->setObjectName(QStringLiteral("lineEdit_Zmax"));
        lineEdit_Zmax->setGeometry(QRect(160, 220, 71, 21));
        label = new QLabel(tab_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 150, 56, 12));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(160, 150, 56, 12));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(160, 200, 56, 12));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(40, 200, 56, 12));
        tabWidget->addTab(tab_2, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        test = new QPushButton(tab);
        test->setObjectName(QStringLiteral("test"));
        test->setGeometry(QRect(60, 30, 171, 51));
        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        tabWidget->addTab(tab_3, QString());

        horizontalLayout->addWidget(tabWidget);

        listWidget = new QListWidget(MainFrame);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        QSizePolicy sizePolicy4(QSizePolicy::Ignored, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(listWidget->sizePolicy().hasHeightForWidth());
        listWidget->setSizePolicy(sizePolicy4);
        listWidget->setMinimumSize(QSize(0, 394));
        QPalette palette1;
        QBrush brush2(QColor(255, 255, 0, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush3(QColor(0, 0, 127, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        QBrush brush4(QColor(120, 120, 120, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        QBrush brush5(QColor(240, 240, 240, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush5);
        listWidget->setPalette(palette1);
        QFont font2;
        font2.setFamily(QStringLiteral("Times New Roman"));
        font2.setPointSize(10);
        listWidget->setFont(font2);

        horizontalLayout->addWidget(listWidget);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(MainFrame);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainFrame);
    } // setupUi

    void retranslateUi(QDialog *MainFrame)
    {
        MainFrame->setWindowTitle(QApplication::translate("MainFrame", "Homeworks", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        buttonOpen->setToolTip(QApplication::translate("MainFrame", "open an image file", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        buttonOpen->setText(QString());
        toolButton_2->setText(QApplication::translate("MainFrame", "...", Q_NULLPTR));
        toolButton_3->setText(QApplication::translate("MainFrame", "...", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        buttonDeleteContents->setToolTip(QApplication::translate("MainFrame", "close all forms", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        buttonDeleteContents->setText(QString());
#ifndef QT_NO_TOOLTIP
        buttonShowList->setToolTip(QApplication::translate("MainFrame", "show the list view", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        buttonShowList->setText(QString());
        buttonAStar->setText(QApplication::translate("MainFrame", "Run A*", Q_NULLPTR));
        label_17->setText(QApplication::translate("MainFrame", "Source", Q_NULLPTR));
        label_18->setText(QApplication::translate("MainFrame", "Goal", Q_NULLPTR));
        editSourceNode->setText(QApplication::translate("MainFrame", "A", Q_NULLPTR));
        editGoalNode->setText(QApplication::translate("MainFrame", "Q", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainFrame", "VG", Q_NULLPTR));
        VG_A_Star->setText(QApplication::translate("MainFrame", "VG_A*", Q_NULLPTR));
        Voronoi->setText(QApplication::translate("MainFrame", "GVD", Q_NULLPTR));
        GVD_A_Star->setText(QApplication::translate("MainFrame", "GVD_A*", Q_NULLPTR));
        Show_Image->setText(QApplication::translate("MainFrame", "Show Result", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_1), QApplication::translate("MainFrame", "1 ", Q_NULLPTR));
        odometry->setText(QApplication::translate("MainFrame", "odometry", Q_NULLPTR));
        potential_fields->setText(QApplication::translate("MainFrame", "potential fields", Q_NULLPTR));
        load_map->setText(QApplication::translate("MainFrame", "load map", Q_NULLPTR));
        Likelihood_Field->setText(QApplication::translate("MainFrame", "likelihood field", Q_NULLPTR));
        label->setText(QApplication::translate("MainFrame", "sigma_hit", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainFrame", "Z_hit", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainFrame", "Z_max", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainFrame", "Z_rand", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainFrame", "2 ", Q_NULLPTR));
        test->setText(QApplication::translate("MainFrame", "test", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainFrame", "3 ", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainFrame", "4", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainFrame: public Ui_MainFrame {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINFRAME_H
